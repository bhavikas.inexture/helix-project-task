# Liferay 6.2 Community Edition GA4 Helix project

Pre requirements for starting Liferay 6.2 CE GA4

1. Download a Liferay Portal. For starters, it's recommended to choose Liferay Portal bundled with Tomcat.
2. Download and install Java (JDK) 8.
3. Download and install MySql

After Starting Liferay 6.2

1. Copy helix-tasks-portlet-1.0.0.war file in liferay-ce-portal-[version]/deploy folder.
2. Crete two site templates and this two site template name must be set in portal-ext.properties file(cusotm.public.pages.template = <public-page-site-template-name>
cusotm.private.pages.template = <private-page-site-template-name>)
3. If already created two site templates just mention template name in portal-ext.properties file.
4. Set porperty for the csv file headers - csv.file.headers = [header_colum1,headercolum2] for the csv file header validation.
5. Add document and media portlet in this two site templates for displaying the uploaded image in the site created.
6. Drage and drop Helix task portlet in your site page.
7. Click on choose file button and upload CSV file.
8. After completing process you get notification on dockbar.
9. Click on Download CSV button download CSV file.
 

