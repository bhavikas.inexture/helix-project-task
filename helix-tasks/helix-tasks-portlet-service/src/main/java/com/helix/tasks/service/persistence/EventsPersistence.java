package com.helix.tasks.service.persistence;

import com.helix.tasks.model.Events;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the events service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsPersistenceImpl
 * @see EventsUtil
 * @generated
 */
public interface EventsPersistence extends BasePersistence<Events> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link EventsUtil} to access the events persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the eventses where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @return the matching eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findBySourceId(
        long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the eventses where sourceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param sourceId the source ID
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @return the range of matching eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findBySourceId(
        long sourceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the eventses where sourceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param sourceId the source ID
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findBySourceId(
        long sourceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first events in the ordered set where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching events
    * @throws com.helix.tasks.NoSuchEventsException if a matching events could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events findBySourceId_First(long sourceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.helix.tasks.NoSuchEventsException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first events in the ordered set where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching events, or <code>null</code> if a matching events could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events fetchBySourceId_First(long sourceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last events in the ordered set where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching events
    * @throws com.helix.tasks.NoSuchEventsException if a matching events could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events findBySourceId_Last(long sourceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.helix.tasks.NoSuchEventsException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last events in the ordered set where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching events, or <code>null</code> if a matching events could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events fetchBySourceId_Last(long sourceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the eventses before and after the current events in the ordered set where sourceId = &#63;.
    *
    * @param eventId the primary key of the current events
    * @param sourceId the source ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next events
    * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events[] findBySourceId_PrevAndNext(
        long eventId, long sourceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.helix.tasks.NoSuchEventsException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the eventses where sourceId = &#63; from the database.
    *
    * @param sourceId the source ID
    * @throws SystemException if a system exception occurred
    */
    public void removeBySourceId(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of eventses where sourceId = &#63;.
    *
    * @param sourceId the source ID
    * @return the number of matching eventses
    * @throws SystemException if a system exception occurred
    */
    public int countBySourceId(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the events in the entity cache if it is enabled.
    *
    * @param events the events
    */
    public void cacheResult(com.helix.tasks.model.Events events);

    /**
    * Caches the eventses in the entity cache if it is enabled.
    *
    * @param eventses the eventses
    */
    public void cacheResult(
        java.util.List<com.helix.tasks.model.Events> eventses);

    /**
    * Creates a new events with the primary key. Does not add the events to the database.
    *
    * @param eventId the primary key for the new events
    * @return the new events
    */
    public com.helix.tasks.model.Events create(long eventId);

    /**
    * Removes the events with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param eventId the primary key of the events
    * @return the events that was removed
    * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events remove(long eventId)
        throws com.helix.tasks.NoSuchEventsException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.helix.tasks.model.Events updateImpl(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the events with the primary key or throws a {@link com.helix.tasks.NoSuchEventsException} if it could not be found.
    *
    * @param eventId the primary key of the events
    * @return the events
    * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events findByPrimaryKey(long eventId)
        throws com.helix.tasks.NoSuchEventsException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the events with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param eventId the primary key of the events
    * @return the events, or <code>null</code> if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Events fetchByPrimaryKey(long eventId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the eventses.
    *
    * @return the eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the eventses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @return the range of eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the eventses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of eventses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Events> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the eventses from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of eventses.
    *
    * @return the number of eventses
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
