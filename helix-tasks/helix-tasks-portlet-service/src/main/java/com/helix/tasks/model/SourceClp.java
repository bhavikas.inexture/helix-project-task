package com.helix.tasks.model;

import com.helix.tasks.service.ClpSerializer;
import com.helix.tasks.service.SourceLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SourceClp extends BaseModelImpl<Source> implements Source {
    private long _sourceId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userUuid;
    private Date _createDate;
    private Date _modifiedDate;
    private String _eventSource;
    private String _eventSourceLink;
    private String _imageEsl;
    private String _organizer;
    private String _city;
    private String _country;
    private BaseModel<?> _sourceRemoteModel;
    private Class<?> _clpSerializerClass = com.helix.tasks.service.ClpSerializer.class;

    public SourceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Source.class;
    }

    @Override
    public String getModelClassName() {
        return Source.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _sourceId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setSourceId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _sourceId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("sourceId", getSourceId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("eventSource", getEventSource());
        attributes.put("eventSourceLink", getEventSourceLink());
        attributes.put("imageEsl", getImageEsl());
        attributes.put("organizer", getOrganizer());
        attributes.put("city", getCity());
        attributes.put("country", getCountry());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long sourceId = (Long) attributes.get("sourceId");

        if (sourceId != null) {
            setSourceId(sourceId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String eventSource = (String) attributes.get("eventSource");

        if (eventSource != null) {
            setEventSource(eventSource);
        }

        String eventSourceLink = (String) attributes.get("eventSourceLink");

        if (eventSourceLink != null) {
            setEventSourceLink(eventSourceLink);
        }

        String imageEsl = (String) attributes.get("imageEsl");

        if (imageEsl != null) {
            setImageEsl(imageEsl);
        }

        String organizer = (String) attributes.get("organizer");

        if (organizer != null) {
            setOrganizer(organizer);
        }

        String city = (String) attributes.get("city");

        if (city != null) {
            setCity(city);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }
    }

    @Override
    public long getSourceId() {
        return _sourceId;
    }

    @Override
    public void setSourceId(long sourceId) {
        _sourceId = sourceId;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setSourceId", long.class);

                method.invoke(_sourceRemoteModel, sourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getGroupId() {
        return _groupId;
    }

    @Override
    public void setGroupId(long groupId) {
        _groupId = groupId;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setGroupId", long.class);

                method.invoke(_sourceRemoteModel, groupId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCompanyId() {
        return _companyId;
    }

    @Override
    public void setCompanyId(long companyId) {
        _companyId = companyId;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCompanyId", long.class);

                method.invoke(_sourceRemoteModel, companyId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_sourceRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_sourceRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_sourceRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEventSource() {
        return _eventSource;
    }

    @Override
    public void setEventSource(String eventSource) {
        _eventSource = eventSource;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setEventSource", String.class);

                method.invoke(_sourceRemoteModel, eventSource);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEventSourceLink() {
        return _eventSourceLink;
    }

    @Override
    public void setEventSourceLink(String eventSourceLink) {
        _eventSourceLink = eventSourceLink;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setEventSourceLink",
                        String.class);

                method.invoke(_sourceRemoteModel, eventSourceLink);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getImageEsl() {
        return _imageEsl;
    }

    @Override
    public void setImageEsl(String imageEsl) {
        _imageEsl = imageEsl;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setImageEsl", String.class);

                method.invoke(_sourceRemoteModel, imageEsl);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getOrganizer() {
        return _organizer;
    }

    @Override
    public void setOrganizer(String organizer) {
        _organizer = organizer;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setOrganizer", String.class);

                method.invoke(_sourceRemoteModel, organizer);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCity() {
        return _city;
    }

    @Override
    public void setCity(String city) {
        _city = city;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCity", String.class);

                method.invoke(_sourceRemoteModel, city);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCountry() {
        return _country;
    }

    @Override
    public void setCountry(String country) {
        _country = country;

        if (_sourceRemoteModel != null) {
            try {
                Class<?> clazz = _sourceRemoteModel.getClass();

                Method method = clazz.getMethod("setCountry", String.class);

                method.invoke(_sourceRemoteModel, country);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getSourceRemoteModel() {
        return _sourceRemoteModel;
    }

    public void setSourceRemoteModel(BaseModel<?> sourceRemoteModel) {
        _sourceRemoteModel = sourceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _sourceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_sourceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            SourceLocalServiceUtil.addSource(this);
        } else {
            SourceLocalServiceUtil.updateSource(this);
        }
    }

    @Override
    public Source toEscapedModel() {
        return (Source) ProxyUtil.newProxyInstance(Source.class.getClassLoader(),
            new Class[] { Source.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        SourceClp clone = new SourceClp();

        clone.setSourceId(getSourceId());
        clone.setGroupId(getGroupId());
        clone.setCompanyId(getCompanyId());
        clone.setUserId(getUserId());
        clone.setCreateDate(getCreateDate());
        clone.setModifiedDate(getModifiedDate());
        clone.setEventSource(getEventSource());
        clone.setEventSourceLink(getEventSourceLink());
        clone.setImageEsl(getImageEsl());
        clone.setOrganizer(getOrganizer());
        clone.setCity(getCity());
        clone.setCountry(getCountry());

        return clone;
    }

    @Override
    public int compareTo(Source source) {
        long primaryKey = source.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SourceClp)) {
            return false;
        }

        SourceClp source = (SourceClp) obj;

        long primaryKey = source.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{sourceId=");
        sb.append(getSourceId());
        sb.append(", groupId=");
        sb.append(getGroupId());
        sb.append(", companyId=");
        sb.append(getCompanyId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", eventSource=");
        sb.append(getEventSource());
        sb.append(", eventSourceLink=");
        sb.append(getEventSourceLink());
        sb.append(", imageEsl=");
        sb.append(getImageEsl());
        sb.append(", organizer=");
        sb.append(getOrganizer());
        sb.append(", city=");
        sb.append(getCity());
        sb.append(", country=");
        sb.append(getCountry());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(40);

        sb.append("<model><model-name>");
        sb.append("com.helix.tasks.model.Source");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>sourceId</column-name><column-value><![CDATA[");
        sb.append(getSourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>groupId</column-name><column-value><![CDATA[");
        sb.append(getGroupId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>companyId</column-name><column-value><![CDATA[");
        sb.append(getCompanyId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>eventSource</column-name><column-value><![CDATA[");
        sb.append(getEventSource());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>eventSourceLink</column-name><column-value><![CDATA[");
        sb.append(getEventSourceLink());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>imageEsl</column-name><column-value><![CDATA[");
        sb.append(getImageEsl());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organizer</column-name><column-value><![CDATA[");
        sb.append(getOrganizer());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>city</column-name><column-value><![CDATA[");
        sb.append(getCity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>country</column-name><column-value><![CDATA[");
        sb.append(getCountry());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
