package com.helix.tasks.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EventsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EventsLocalService
 * @generated
 */
public class EventsLocalServiceWrapper implements EventsLocalService,
    ServiceWrapper<EventsLocalService> {
    private EventsLocalService _eventsLocalService;

    public EventsLocalServiceWrapper(EventsLocalService eventsLocalService) {
        _eventsLocalService = eventsLocalService;
    }

    /**
    * Adds the events to the database. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Events addEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.addEvents(events);
    }

    /**
    * Creates a new events with the primary key. Does not add the events to the database.
    *
    * @param eventId the primary key for the new events
    * @return the new events
    */
    @Override
    public com.helix.tasks.model.Events createEvents(long eventId) {
        return _eventsLocalService.createEvents(eventId);
    }

    /**
    * Deletes the events with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param eventId the primary key of the events
    * @return the events that was removed
    * @throws PortalException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Events deleteEvents(long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.deleteEvents(eventId);
    }

    /**
    * Deletes the events from the database. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Events deleteEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.deleteEvents(events);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _eventsLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public com.helix.tasks.model.Events fetchEvents(long eventId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.fetchEvents(eventId);
    }

    /**
    * Returns the events with the primary key.
    *
    * @param eventId the primary key of the events
    * @return the events
    * @throws PortalException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Events getEvents(long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.getEvents(eventId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the eventses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @return the range of eventses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<com.helix.tasks.model.Events> getEventses(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.getEventses(start, end);
    }

    /**
    * Returns the number of eventses.
    *
    * @return the number of eventses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getEventsesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.getEventsesCount();
    }

    /**
    * Updates the events in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Events updateEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.updateEvents(events);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _eventsLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _eventsLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _eventsLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public com.helix.tasks.model.Events addEvent(long companyId, long groupId,
        long userId, long sourceId, java.lang.String eventUrl,
        java.lang.String eventName, java.lang.String eventTime,
        java.lang.String eventLocation, java.lang.String imageEu,
        java.lang.String relativeLinkToSite, java.lang.String siteUuid,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.addEvent(companyId, groupId, userId,
            sourceId, eventUrl, eventName, eventTime, eventLocation, imageEu,
            relativeLinkToSite, siteUuid, serviceContext);
    }

    @Override
    public java.util.List<com.helix.tasks.model.Events> findBySourceId(
        long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _eventsLocalService.findBySourceId(sourceId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public EventsLocalService getWrappedEventsLocalService() {
        return _eventsLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedEventsLocalService(
        EventsLocalService eventsLocalService) {
        _eventsLocalService = eventsLocalService;
    }

    @Override
    public EventsLocalService getWrappedService() {
        return _eventsLocalService;
    }

    @Override
    public void setWrappedService(EventsLocalService eventsLocalService) {
        _eventsLocalService = eventsLocalService;
    }
}
