package com.helix.tasks.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EventsSoap implements Serializable {
    private long _eventId;
    private long _sourceId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private Date _createDate;
    private Date _modifiedDate;
    private String _url;
    private String _name;
    private String _time;
    private String _location;
    private String _imageEu;
    private String _relativeLinkToSite;
    private String _siteUuid;

    public EventsSoap() {
    }

    public static EventsSoap toSoapModel(Events model) {
        EventsSoap soapModel = new EventsSoap();

        soapModel.setEventId(model.getEventId());
        soapModel.setSourceId(model.getSourceId());
        soapModel.setGroupId(model.getGroupId());
        soapModel.setCompanyId(model.getCompanyId());
        soapModel.setUserId(model.getUserId());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setUrl(model.getUrl());
        soapModel.setName(model.getName());
        soapModel.setTime(model.getTime());
        soapModel.setLocation(model.getLocation());
        soapModel.setImageEu(model.getImageEu());
        soapModel.setRelativeLinkToSite(model.getRelativeLinkToSite());
        soapModel.setSiteUuid(model.getSiteUuid());

        return soapModel;
    }

    public static EventsSoap[] toSoapModels(Events[] models) {
        EventsSoap[] soapModels = new EventsSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static EventsSoap[][] toSoapModels(Events[][] models) {
        EventsSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new EventsSoap[models.length][models[0].length];
        } else {
            soapModels = new EventsSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static EventsSoap[] toSoapModels(List<Events> models) {
        List<EventsSoap> soapModels = new ArrayList<EventsSoap>(models.size());

        for (Events model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new EventsSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _eventId;
    }

    public void setPrimaryKey(long pk) {
        setEventId(pk);
    }

    public long getEventId() {
        return _eventId;
    }

    public void setEventId(long eventId) {
        _eventId = eventId;
    }

    public long getSourceId() {
        return _sourceId;
    }

    public void setSourceId(long sourceId) {
        _sourceId = sourceId;
    }

    public long getGroupId() {
        return _groupId;
    }

    public void setGroupId(long groupId) {
        _groupId = groupId;
    }

    public long getCompanyId() {
        return _companyId;
    }

    public void setCompanyId(long companyId) {
        _companyId = companyId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String url) {
        _url = url;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getTime() {
        return _time;
    }

    public void setTime(String time) {
        _time = time;
    }

    public String getLocation() {
        return _location;
    }

    public void setLocation(String location) {
        _location = location;
    }

    public String getImageEu() {
        return _imageEu;
    }

    public void setImageEu(String imageEu) {
        _imageEu = imageEu;
    }

    public String getRelativeLinkToSite() {
        return _relativeLinkToSite;
    }

    public void setRelativeLinkToSite(String relativeLinkToSite) {
        _relativeLinkToSite = relativeLinkToSite;
    }

    public String getSiteUuid() {
        return _siteUuid;
    }

    public void setSiteUuid(String siteUuid) {
        _siteUuid = siteUuid;
    }
}
