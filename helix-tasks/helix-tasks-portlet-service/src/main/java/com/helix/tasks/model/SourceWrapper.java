package com.helix.tasks.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Source}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Source
 * @generated
 */
public class SourceWrapper implements Source, ModelWrapper<Source> {
    private Source _source;

    public SourceWrapper(Source source) {
        _source = source;
    }

    @Override
    public Class<?> getModelClass() {
        return Source.class;
    }

    @Override
    public String getModelClassName() {
        return Source.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("sourceId", getSourceId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("eventSource", getEventSource());
        attributes.put("eventSourceLink", getEventSourceLink());
        attributes.put("imageEsl", getImageEsl());
        attributes.put("organizer", getOrganizer());
        attributes.put("city", getCity());
        attributes.put("country", getCountry());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long sourceId = (Long) attributes.get("sourceId");

        if (sourceId != null) {
            setSourceId(sourceId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String eventSource = (String) attributes.get("eventSource");

        if (eventSource != null) {
            setEventSource(eventSource);
        }

        String eventSourceLink = (String) attributes.get("eventSourceLink");

        if (eventSourceLink != null) {
            setEventSourceLink(eventSourceLink);
        }

        String imageEsl = (String) attributes.get("imageEsl");

        if (imageEsl != null) {
            setImageEsl(imageEsl);
        }

        String organizer = (String) attributes.get("organizer");

        if (organizer != null) {
            setOrganizer(organizer);
        }

        String city = (String) attributes.get("city");

        if (city != null) {
            setCity(city);
        }

        String country = (String) attributes.get("country");

        if (country != null) {
            setCountry(country);
        }
    }

    /**
    * Returns the primary key of this source.
    *
    * @return the primary key of this source
    */
    @Override
    public long getPrimaryKey() {
        return _source.getPrimaryKey();
    }

    /**
    * Sets the primary key of this source.
    *
    * @param primaryKey the primary key of this source
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _source.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the source ID of this source.
    *
    * @return the source ID of this source
    */
    @Override
    public long getSourceId() {
        return _source.getSourceId();
    }

    /**
    * Sets the source ID of this source.
    *
    * @param sourceId the source ID of this source
    */
    @Override
    public void setSourceId(long sourceId) {
        _source.setSourceId(sourceId);
    }

    /**
    * Returns the group ID of this source.
    *
    * @return the group ID of this source
    */
    @Override
    public long getGroupId() {
        return _source.getGroupId();
    }

    /**
    * Sets the group ID of this source.
    *
    * @param groupId the group ID of this source
    */
    @Override
    public void setGroupId(long groupId) {
        _source.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this source.
    *
    * @return the company ID of this source
    */
    @Override
    public long getCompanyId() {
        return _source.getCompanyId();
    }

    /**
    * Sets the company ID of this source.
    *
    * @param companyId the company ID of this source
    */
    @Override
    public void setCompanyId(long companyId) {
        _source.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this source.
    *
    * @return the user ID of this source
    */
    @Override
    public long getUserId() {
        return _source.getUserId();
    }

    /**
    * Sets the user ID of this source.
    *
    * @param userId the user ID of this source
    */
    @Override
    public void setUserId(long userId) {
        _source.setUserId(userId);
    }

    /**
    * Returns the user uuid of this source.
    *
    * @return the user uuid of this source
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _source.getUserUuid();
    }

    /**
    * Sets the user uuid of this source.
    *
    * @param userUuid the user uuid of this source
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _source.setUserUuid(userUuid);
    }

    /**
    * Returns the create date of this source.
    *
    * @return the create date of this source
    */
    @Override
    public java.util.Date getCreateDate() {
        return _source.getCreateDate();
    }

    /**
    * Sets the create date of this source.
    *
    * @param createDate the create date of this source
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _source.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this source.
    *
    * @return the modified date of this source
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _source.getModifiedDate();
    }

    /**
    * Sets the modified date of this source.
    *
    * @param modifiedDate the modified date of this source
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _source.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the event source of this source.
    *
    * @return the event source of this source
    */
    @Override
    public java.lang.String getEventSource() {
        return _source.getEventSource();
    }

    /**
    * Sets the event source of this source.
    *
    * @param eventSource the event source of this source
    */
    @Override
    public void setEventSource(java.lang.String eventSource) {
        _source.setEventSource(eventSource);
    }

    /**
    * Returns the event source link of this source.
    *
    * @return the event source link of this source
    */
    @Override
    public java.lang.String getEventSourceLink() {
        return _source.getEventSourceLink();
    }

    /**
    * Sets the event source link of this source.
    *
    * @param eventSourceLink the event source link of this source
    */
    @Override
    public void setEventSourceLink(java.lang.String eventSourceLink) {
        _source.setEventSourceLink(eventSourceLink);
    }

    /**
    * Returns the image esl of this source.
    *
    * @return the image esl of this source
    */
    @Override
    public java.lang.String getImageEsl() {
        return _source.getImageEsl();
    }

    /**
    * Sets the image esl of this source.
    *
    * @param imageEsl the image esl of this source
    */
    @Override
    public void setImageEsl(java.lang.String imageEsl) {
        _source.setImageEsl(imageEsl);
    }

    /**
    * Returns the organizer of this source.
    *
    * @return the organizer of this source
    */
    @Override
    public java.lang.String getOrganizer() {
        return _source.getOrganizer();
    }

    /**
    * Sets the organizer of this source.
    *
    * @param organizer the organizer of this source
    */
    @Override
    public void setOrganizer(java.lang.String organizer) {
        _source.setOrganizer(organizer);
    }

    /**
    * Returns the city of this source.
    *
    * @return the city of this source
    */
    @Override
    public java.lang.String getCity() {
        return _source.getCity();
    }

    /**
    * Sets the city of this source.
    *
    * @param city the city of this source
    */
    @Override
    public void setCity(java.lang.String city) {
        _source.setCity(city);
    }

    /**
    * Returns the country of this source.
    *
    * @return the country of this source
    */
    @Override
    public java.lang.String getCountry() {
        return _source.getCountry();
    }

    /**
    * Sets the country of this source.
    *
    * @param country the country of this source
    */
    @Override
    public void setCountry(java.lang.String country) {
        _source.setCountry(country);
    }

    @Override
    public boolean isNew() {
        return _source.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _source.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _source.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _source.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _source.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _source.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _source.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _source.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _source.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _source.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _source.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new SourceWrapper((Source) _source.clone());
    }

    @Override
    public int compareTo(com.helix.tasks.model.Source source) {
        return _source.compareTo(source);
    }

    @Override
    public int hashCode() {
        return _source.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.helix.tasks.model.Source> toCacheModel() {
        return _source.toCacheModel();
    }

    @Override
    public com.helix.tasks.model.Source toEscapedModel() {
        return new SourceWrapper(_source.toEscapedModel());
    }

    @Override
    public com.helix.tasks.model.Source toUnescapedModel() {
        return new SourceWrapper(_source.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _source.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _source.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _source.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SourceWrapper)) {
            return false;
        }

        SourceWrapper sourceWrapper = (SourceWrapper) obj;

        if (Validator.equals(_source, sourceWrapper._source)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Source getWrappedSource() {
        return _source;
    }

    @Override
    public Source getWrappedModel() {
        return _source;
    }

    @Override
    public void resetOriginalValues() {
        _source.resetOriginalValues();
    }
}
