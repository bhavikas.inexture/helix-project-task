package com.helix.tasks.service.persistence;

import com.helix.tasks.model.Source;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the source service. This utility wraps {@link SourcePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SourcePersistence
 * @see SourcePersistenceImpl
 * @generated
 */
public class SourceUtil {
    private static SourcePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Source source) {
        getPersistence().clearCache(source);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Source> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Source> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Source> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Source update(Source source) throws SystemException {
        return getPersistence().update(source);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Source update(Source source, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(source, serviceContext);
    }

    /**
    * Returns the source where eventSourceLink = &#63; or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
    *
    * @param eventSourceLink the event source link
    * @return the matching source
    * @throws com.helix.tasks.NoSuchSourceException if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source findByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByEventSourceLink(eventSourceLink);
    }

    /**
    * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param eventSourceLink the event source link
    * @return the matching source, or <code>null</code> if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source fetchByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByEventSourceLink(eventSourceLink);
    }

    /**
    * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param eventSourceLink the event source link
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching source, or <code>null</code> if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source fetchByEventSourceLink(
        java.lang.String eventSourceLink, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByEventSourceLink(eventSourceLink, retrieveFromCache);
    }

    /**
    * Removes the source where eventSourceLink = &#63; from the database.
    *
    * @param eventSourceLink the event source link
    * @return the source that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source removeByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByEventSourceLink(eventSourceLink);
    }

    /**
    * Returns the number of sources where eventSourceLink = &#63;.
    *
    * @param eventSourceLink the event source link
    * @return the number of matching sources
    * @throws SystemException if a system exception occurred
    */
    public static int countByEventSourceLink(java.lang.String eventSourceLink)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByEventSourceLink(eventSourceLink);
    }

    /**
    * Caches the source in the entity cache if it is enabled.
    *
    * @param source the source
    */
    public static void cacheResult(com.helix.tasks.model.Source source) {
        getPersistence().cacheResult(source);
    }

    /**
    * Caches the sources in the entity cache if it is enabled.
    *
    * @param sources the sources
    */
    public static void cacheResult(
        java.util.List<com.helix.tasks.model.Source> sources) {
        getPersistence().cacheResult(sources);
    }

    /**
    * Creates a new source with the primary key. Does not add the source to the database.
    *
    * @param sourceId the primary key for the new source
    * @return the new source
    */
    public static com.helix.tasks.model.Source create(long sourceId) {
        return getPersistence().create(sourceId);
    }

    /**
    * Removes the source with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param sourceId the primary key of the source
    * @return the source that was removed
    * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source remove(long sourceId)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(sourceId);
    }

    public static com.helix.tasks.model.Source updateImpl(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(source);
    }

    /**
    * Returns the source with the primary key or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
    *
    * @param sourceId the primary key of the source
    * @return the source
    * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source findByPrimaryKey(long sourceId)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(sourceId);
    }

    /**
    * Returns the source with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param sourceId the primary key of the source
    * @return the source, or <code>null</code> if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source fetchByPrimaryKey(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(sourceId);
    }

    /**
    * Returns all the sources.
    *
    * @return the sources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.helix.tasks.model.Source> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @return the range of sources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.helix.tasks.model.Source> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.helix.tasks.model.Source> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the sources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of sources.
    *
    * @return the number of sources
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static SourcePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (SourcePersistence) PortletBeanLocatorUtil.locate(com.helix.tasks.service.ClpSerializer.getServletContextName(),
                    SourcePersistence.class.getName());

            ReferenceRegistry.registerReference(SourceUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(SourcePersistence persistence) {
    }
}
