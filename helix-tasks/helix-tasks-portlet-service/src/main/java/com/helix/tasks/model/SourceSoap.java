package com.helix.tasks.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class SourceSoap implements Serializable {
    private long _sourceId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private Date _createDate;
    private Date _modifiedDate;
    private String _eventSource;
    private String _eventSourceLink;
    private String _imageEsl;
    private String _organizer;
    private String _city;
    private String _country;

    public SourceSoap() {
    }

    public static SourceSoap toSoapModel(Source model) {
        SourceSoap soapModel = new SourceSoap();

        soapModel.setSourceId(model.getSourceId());
        soapModel.setGroupId(model.getGroupId());
        soapModel.setCompanyId(model.getCompanyId());
        soapModel.setUserId(model.getUserId());
        soapModel.setCreateDate(model.getCreateDate());
        soapModel.setModifiedDate(model.getModifiedDate());
        soapModel.setEventSource(model.getEventSource());
        soapModel.setEventSourceLink(model.getEventSourceLink());
        soapModel.setImageEsl(model.getImageEsl());
        soapModel.setOrganizer(model.getOrganizer());
        soapModel.setCity(model.getCity());
        soapModel.setCountry(model.getCountry());

        return soapModel;
    }

    public static SourceSoap[] toSoapModels(Source[] models) {
        SourceSoap[] soapModels = new SourceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static SourceSoap[][] toSoapModels(Source[][] models) {
        SourceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new SourceSoap[models.length][models[0].length];
        } else {
            soapModels = new SourceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static SourceSoap[] toSoapModels(List<Source> models) {
        List<SourceSoap> soapModels = new ArrayList<SourceSoap>(models.size());

        for (Source model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new SourceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _sourceId;
    }

    public void setPrimaryKey(long pk) {
        setSourceId(pk);
    }

    public long getSourceId() {
        return _sourceId;
    }

    public void setSourceId(long sourceId) {
        _sourceId = sourceId;
    }

    public long getGroupId() {
        return _groupId;
    }

    public void setGroupId(long groupId) {
        _groupId = groupId;
    }

    public long getCompanyId() {
        return _companyId;
    }

    public void setCompanyId(long companyId) {
        _companyId = companyId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public Date getCreateDate() {
        return _createDate;
    }

    public void setCreateDate(Date createDate) {
        _createDate = createDate;
    }

    public Date getModifiedDate() {
        return _modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;
    }

    public String getEventSource() {
        return _eventSource;
    }

    public void setEventSource(String eventSource) {
        _eventSource = eventSource;
    }

    public String getEventSourceLink() {
        return _eventSourceLink;
    }

    public void setEventSourceLink(String eventSourceLink) {
        _eventSourceLink = eventSourceLink;
    }

    public String getImageEsl() {
        return _imageEsl;
    }

    public void setImageEsl(String imageEsl) {
        _imageEsl = imageEsl;
    }

    public String getOrganizer() {
        return _organizer;
    }

    public void setOrganizer(String organizer) {
        _organizer = organizer;
    }

    public String getCity() {
        return _city;
    }

    public void setCity(String city) {
        _city = city;
    }

    public String getCountry() {
        return _country;
    }

    public void setCountry(String country) {
        _country = country;
    }
}
