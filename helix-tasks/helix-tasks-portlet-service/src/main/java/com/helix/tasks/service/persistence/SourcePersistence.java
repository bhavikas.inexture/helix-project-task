package com.helix.tasks.service.persistence;

import com.helix.tasks.model.Source;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the source service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SourcePersistenceImpl
 * @see SourceUtil
 * @generated
 */
public interface SourcePersistence extends BasePersistence<Source> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link SourceUtil} to access the source persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the source where eventSourceLink = &#63; or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
    *
    * @param eventSourceLink the event source link
    * @return the matching source
    * @throws com.helix.tasks.NoSuchSourceException if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source findByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param eventSourceLink the event source link
    * @return the matching source, or <code>null</code> if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source fetchByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param eventSourceLink the event source link
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching source, or <code>null</code> if a matching source could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source fetchByEventSourceLink(
        java.lang.String eventSourceLink, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the source where eventSourceLink = &#63; from the database.
    *
    * @param eventSourceLink the event source link
    * @return the source that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source removeByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of sources where eventSourceLink = &#63;.
    *
    * @param eventSourceLink the event source link
    * @return the number of matching sources
    * @throws SystemException if a system exception occurred
    */
    public int countByEventSourceLink(java.lang.String eventSourceLink)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the source in the entity cache if it is enabled.
    *
    * @param source the source
    */
    public void cacheResult(com.helix.tasks.model.Source source);

    /**
    * Caches the sources in the entity cache if it is enabled.
    *
    * @param sources the sources
    */
    public void cacheResult(
        java.util.List<com.helix.tasks.model.Source> sources);

    /**
    * Creates a new source with the primary key. Does not add the source to the database.
    *
    * @param sourceId the primary key for the new source
    * @return the new source
    */
    public com.helix.tasks.model.Source create(long sourceId);

    /**
    * Removes the source with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param sourceId the primary key of the source
    * @return the source that was removed
    * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source remove(long sourceId)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.helix.tasks.model.Source updateImpl(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the source with the primary key or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
    *
    * @param sourceId the primary key of the source
    * @return the source
    * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source findByPrimaryKey(long sourceId)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the source with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param sourceId the primary key of the source
    * @return the source, or <code>null</code> if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.helix.tasks.model.Source fetchByPrimaryKey(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the sources.
    *
    * @return the sources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Source> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @return the range of sources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Source> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sources
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.helix.tasks.model.Source> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the sources from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of sources.
    *
    * @return the number of sources
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
