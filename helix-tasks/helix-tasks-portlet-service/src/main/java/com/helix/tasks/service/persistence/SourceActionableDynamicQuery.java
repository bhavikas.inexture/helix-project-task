package com.helix.tasks.service.persistence;

import com.helix.tasks.model.Source;
import com.helix.tasks.service.SourceLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class SourceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public SourceActionableDynamicQuery() throws SystemException {
        setBaseLocalService(SourceLocalServiceUtil.getService());
        setClass(Source.class);

        setClassLoader(com.helix.tasks.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("sourceId");
    }
}
