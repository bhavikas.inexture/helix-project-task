package com.helix.tasks.model;

import com.helix.tasks.service.ClpSerializer;
import com.helix.tasks.service.EventsLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class EventsClp extends BaseModelImpl<Events> implements Events {
    private long _eventId;
    private long _sourceId;
    private long _groupId;
    private long _companyId;
    private long _userId;
    private String _userUuid;
    private Date _createDate;
    private Date _modifiedDate;
    private String _url;
    private String _name;
    private String _time;
    private String _location;
    private String _imageEu;
    private String _relativeLinkToSite;
    private String _siteUuid;
    private BaseModel<?> _eventsRemoteModel;
    private Class<?> _clpSerializerClass = com.helix.tasks.service.ClpSerializer.class;

    public EventsClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Events.class;
    }

    @Override
    public String getModelClassName() {
        return Events.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _eventId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setEventId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _eventId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("eventId", getEventId());
        attributes.put("sourceId", getSourceId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("url", getUrl());
        attributes.put("name", getName());
        attributes.put("time", getTime());
        attributes.put("location", getLocation());
        attributes.put("imageEu", getImageEu());
        attributes.put("relativeLinkToSite", getRelativeLinkToSite());
        attributes.put("siteUuid", getSiteUuid());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long eventId = (Long) attributes.get("eventId");

        if (eventId != null) {
            setEventId(eventId);
        }

        Long sourceId = (Long) attributes.get("sourceId");

        if (sourceId != null) {
            setSourceId(sourceId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String url = (String) attributes.get("url");

        if (url != null) {
            setUrl(url);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String time = (String) attributes.get("time");

        if (time != null) {
            setTime(time);
        }

        String location = (String) attributes.get("location");

        if (location != null) {
            setLocation(location);
        }

        String imageEu = (String) attributes.get("imageEu");

        if (imageEu != null) {
            setImageEu(imageEu);
        }

        String relativeLinkToSite = (String) attributes.get(
                "relativeLinkToSite");

        if (relativeLinkToSite != null) {
            setRelativeLinkToSite(relativeLinkToSite);
        }

        String siteUuid = (String) attributes.get("siteUuid");

        if (siteUuid != null) {
            setSiteUuid(siteUuid);
        }
    }

    @Override
    public long getEventId() {
        return _eventId;
    }

    @Override
    public void setEventId(long eventId) {
        _eventId = eventId;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setEventId", long.class);

                method.invoke(_eventsRemoteModel, eventId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSourceId() {
        return _sourceId;
    }

    @Override
    public void setSourceId(long sourceId) {
        _sourceId = sourceId;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setSourceId", long.class);

                method.invoke(_eventsRemoteModel, sourceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getGroupId() {
        return _groupId;
    }

    @Override
    public void setGroupId(long groupId) {
        _groupId = groupId;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setGroupId", long.class);

                method.invoke(_eventsRemoteModel, groupId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getCompanyId() {
        return _companyId;
    }

    @Override
    public void setCompanyId(long companyId) {
        _companyId = companyId;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setCompanyId", long.class);

                method.invoke(_eventsRemoteModel, companyId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_eventsRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public Date getCreateDate() {
        return _createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        _createDate = createDate;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setCreateDate", Date.class);

                method.invoke(_eventsRemoteModel, createDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getModifiedDate() {
        return _modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        _modifiedDate = modifiedDate;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setModifiedDate", Date.class);

                method.invoke(_eventsRemoteModel, modifiedDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUrl() {
        return _url;
    }

    @Override
    public void setUrl(String url) {
        _url = url;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setUrl", String.class);

                method.invoke(_eventsRemoteModel, url);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_eventsRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTime() {
        return _time;
    }

    @Override
    public void setTime(String time) {
        _time = time;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setTime", String.class);

                method.invoke(_eventsRemoteModel, time);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLocation() {
        return _location;
    }

    @Override
    public void setLocation(String location) {
        _location = location;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setLocation", String.class);

                method.invoke(_eventsRemoteModel, location);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getImageEu() {
        return _imageEu;
    }

    @Override
    public void setImageEu(String imageEu) {
        _imageEu = imageEu;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setImageEu", String.class);

                method.invoke(_eventsRemoteModel, imageEu);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRelativeLinkToSite() {
        return _relativeLinkToSite;
    }

    @Override
    public void setRelativeLinkToSite(String relativeLinkToSite) {
        _relativeLinkToSite = relativeLinkToSite;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setRelativeLinkToSite",
                        String.class);

                method.invoke(_eventsRemoteModel, relativeLinkToSite);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSiteUuid() {
        return _siteUuid;
    }

    @Override
    public void setSiteUuid(String siteUuid) {
        _siteUuid = siteUuid;

        if (_eventsRemoteModel != null) {
            try {
                Class<?> clazz = _eventsRemoteModel.getClass();

                Method method = clazz.getMethod("setSiteUuid", String.class);

                method.invoke(_eventsRemoteModel, siteUuid);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getEventsRemoteModel() {
        return _eventsRemoteModel;
    }

    public void setEventsRemoteModel(BaseModel<?> eventsRemoteModel) {
        _eventsRemoteModel = eventsRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _eventsRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_eventsRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            EventsLocalServiceUtil.addEvents(this);
        } else {
            EventsLocalServiceUtil.updateEvents(this);
        }
    }

    @Override
    public Events toEscapedModel() {
        return (Events) ProxyUtil.newProxyInstance(Events.class.getClassLoader(),
            new Class[] { Events.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        EventsClp clone = new EventsClp();

        clone.setEventId(getEventId());
        clone.setSourceId(getSourceId());
        clone.setGroupId(getGroupId());
        clone.setCompanyId(getCompanyId());
        clone.setUserId(getUserId());
        clone.setCreateDate(getCreateDate());
        clone.setModifiedDate(getModifiedDate());
        clone.setUrl(getUrl());
        clone.setName(getName());
        clone.setTime(getTime());
        clone.setLocation(getLocation());
        clone.setImageEu(getImageEu());
        clone.setRelativeLinkToSite(getRelativeLinkToSite());
        clone.setSiteUuid(getSiteUuid());

        return clone;
    }

    @Override
    public int compareTo(Events events) {
        long primaryKey = events.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EventsClp)) {
            return false;
        }

        EventsClp events = (EventsClp) obj;

        long primaryKey = events.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(29);

        sb.append("{eventId=");
        sb.append(getEventId());
        sb.append(", sourceId=");
        sb.append(getSourceId());
        sb.append(", groupId=");
        sb.append(getGroupId());
        sb.append(", companyId=");
        sb.append(getCompanyId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", createDate=");
        sb.append(getCreateDate());
        sb.append(", modifiedDate=");
        sb.append(getModifiedDate());
        sb.append(", url=");
        sb.append(getUrl());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", time=");
        sb.append(getTime());
        sb.append(", location=");
        sb.append(getLocation());
        sb.append(", imageEu=");
        sb.append(getImageEu());
        sb.append(", relativeLinkToSite=");
        sb.append(getRelativeLinkToSite());
        sb.append(", siteUuid=");
        sb.append(getSiteUuid());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(46);

        sb.append("<model><model-name>");
        sb.append("com.helix.tasks.model.Events");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>eventId</column-name><column-value><![CDATA[");
        sb.append(getEventId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sourceId</column-name><column-value><![CDATA[");
        sb.append(getSourceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>groupId</column-name><column-value><![CDATA[");
        sb.append(getGroupId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>companyId</column-name><column-value><![CDATA[");
        sb.append(getCompanyId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createDate</column-name><column-value><![CDATA[");
        sb.append(getCreateDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
        sb.append(getModifiedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>url</column-name><column-value><![CDATA[");
        sb.append(getUrl());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>time</column-name><column-value><![CDATA[");
        sb.append(getTime());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>location</column-name><column-value><![CDATA[");
        sb.append(getLocation());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>imageEu</column-name><column-value><![CDATA[");
        sb.append(getImageEu());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>relativeLinkToSite</column-name><column-value><![CDATA[");
        sb.append(getRelativeLinkToSite());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>siteUuid</column-name><column-value><![CDATA[");
        sb.append(getSiteUuid());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
