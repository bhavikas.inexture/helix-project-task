package com.helix.tasks.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Source. This utility wraps
 * {@link com.helix.tasks.service.impl.SourceLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see SourceLocalService
 * @see com.helix.tasks.service.base.SourceLocalServiceBaseImpl
 * @see com.helix.tasks.service.impl.SourceLocalServiceImpl
 * @generated
 */
public class SourceLocalServiceUtil {
    private static SourceLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.helix.tasks.service.impl.SourceLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the source to the database. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source addSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addSource(source);
    }

    /**
    * Creates a new source with the primary key. Does not add the source to the database.
    *
    * @param sourceId the primary key for the new source
    * @return the new source
    */
    public static com.helix.tasks.model.Source createSource(long sourceId) {
        return getService().createSource(sourceId);
    }

    /**
    * Deletes the source with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param sourceId the primary key of the source
    * @return the source that was removed
    * @throws PortalException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source deleteSource(long sourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteSource(sourceId);
    }

    /**
    * Deletes the source from the database. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source deleteSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteSource(source);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static com.helix.tasks.model.Source fetchSource(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchSource(sourceId);
    }

    /**
    * Returns the source with the primary key.
    *
    * @param sourceId the primary key of the source
    * @return the source
    * @throws PortalException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source getSource(long sourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getSource(sourceId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @return the range of sources
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.helix.tasks.model.Source> getSources(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSources(start, end);
    }

    /**
    * Returns the number of sources.
    *
    * @return the number of sources
    * @throws SystemException if a system exception occurred
    */
    public static int getSourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSourcesCount();
    }

    /**
    * Updates the source in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Source updateSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateSource(source);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.helix.tasks.model.Source addSource(long companyId,
        long groupId, long userId, java.lang.String eventSource,
        java.lang.String eventSourceLink, java.lang.String imageEsl,
        java.lang.String organizer, java.lang.String city,
        java.lang.String country,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addSource(companyId, groupId, userId, eventSource,
            eventSourceLink, imageEsl, organizer, city, country, serviceContext);
    }

    public static com.helix.tasks.model.Source findByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().findByEventSourceLink(eventSourceLink);
    }

    public static void clearService() {
        _service = null;
    }

    public static SourceLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    SourceLocalService.class.getName());

            if (invokableLocalService instanceof SourceLocalService) {
                _service = (SourceLocalService) invokableLocalService;
            } else {
                _service = new SourceLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(SourceLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(SourceLocalService service) {
    }
}
