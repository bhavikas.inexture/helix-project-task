package com.helix.tasks.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SourceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SourceLocalService
 * @generated
 */
public class SourceLocalServiceWrapper implements SourceLocalService,
    ServiceWrapper<SourceLocalService> {
    private SourceLocalService _sourceLocalService;

    public SourceLocalServiceWrapper(SourceLocalService sourceLocalService) {
        _sourceLocalService = sourceLocalService;
    }

    /**
    * Adds the source to the database. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Source addSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.addSource(source);
    }

    /**
    * Creates a new source with the primary key. Does not add the source to the database.
    *
    * @param sourceId the primary key for the new source
    * @return the new source
    */
    @Override
    public com.helix.tasks.model.Source createSource(long sourceId) {
        return _sourceLocalService.createSource(sourceId);
    }

    /**
    * Deletes the source with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param sourceId the primary key of the source
    * @return the source that was removed
    * @throws PortalException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Source deleteSource(long sourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.deleteSource(sourceId);
    }

    /**
    * Deletes the source from the database. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Source deleteSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.deleteSource(source);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _sourceLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public com.helix.tasks.model.Source fetchSource(long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.fetchSource(sourceId);
    }

    /**
    * Returns the source with the primary key.
    *
    * @param sourceId the primary key of the source
    * @return the source
    * @throws PortalException if a source with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Source getSource(long sourceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.getSource(sourceId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the sources.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of sources
    * @param end the upper bound of the range of sources (not inclusive)
    * @return the range of sources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<com.helix.tasks.model.Source> getSources(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.getSources(start, end);
    }

    /**
    * Returns the number of sources.
    *
    * @return the number of sources
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getSourcesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.getSourcesCount();
    }

    /**
    * Updates the source in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param source the source
    * @return the source that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.helix.tasks.model.Source updateSource(
        com.helix.tasks.model.Source source)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.updateSource(source);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _sourceLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _sourceLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _sourceLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public com.helix.tasks.model.Source addSource(long companyId, long groupId,
        long userId, java.lang.String eventSource,
        java.lang.String eventSourceLink, java.lang.String imageEsl,
        java.lang.String organizer, java.lang.String city,
        java.lang.String country,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.addSource(companyId, groupId, userId,
            eventSource, eventSourceLink, imageEsl, organizer, city, country,
            serviceContext);
    }

    @Override
    public com.helix.tasks.model.Source findByEventSourceLink(
        java.lang.String eventSourceLink)
        throws com.helix.tasks.NoSuchSourceException,
            com.liferay.portal.kernel.exception.SystemException {
        return _sourceLocalService.findByEventSourceLink(eventSourceLink);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public SourceLocalService getWrappedSourceLocalService() {
        return _sourceLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedSourceLocalService(
        SourceLocalService sourceLocalService) {
        _sourceLocalService = sourceLocalService;
    }

    @Override
    public SourceLocalService getWrappedService() {
        return _sourceLocalService;
    }

    @Override
    public void setWrappedService(SourceLocalService sourceLocalService) {
        _sourceLocalService = sourceLocalService;
    }
}
