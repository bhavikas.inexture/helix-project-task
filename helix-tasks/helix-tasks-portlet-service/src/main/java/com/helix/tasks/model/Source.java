package com.helix.tasks.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Source service. Represents a row in the &quot;Helix_Source&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see SourceModel
 * @see com.helix.tasks.model.impl.SourceImpl
 * @see com.helix.tasks.model.impl.SourceModelImpl
 * @generated
 */
public interface Source extends SourceModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.helix.tasks.model.impl.SourceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
