package com.helix.tasks.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Events. This utility wraps
 * {@link com.helix.tasks.service.impl.EventsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventsLocalService
 * @see com.helix.tasks.service.base.EventsLocalServiceBaseImpl
 * @see com.helix.tasks.service.impl.EventsLocalServiceImpl
 * @generated
 */
public class EventsLocalServiceUtil {
    private static EventsLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.helix.tasks.service.impl.EventsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the events to the database. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Events addEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addEvents(events);
    }

    /**
    * Creates a new events with the primary key. Does not add the events to the database.
    *
    * @param eventId the primary key for the new events
    * @return the new events
    */
    public static com.helix.tasks.model.Events createEvents(long eventId) {
        return getService().createEvents(eventId);
    }

    /**
    * Deletes the events with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param eventId the primary key of the events
    * @return the events that was removed
    * @throws PortalException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Events deleteEvents(long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteEvents(eventId);
    }

    /**
    * Deletes the events from the database. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Events deleteEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteEvents(events);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static com.helix.tasks.model.Events fetchEvents(long eventId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchEvents(eventId);
    }

    /**
    * Returns the events with the primary key.
    *
    * @param eventId the primary key of the events
    * @return the events
    * @throws PortalException if a events with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Events getEvents(long eventId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getEvents(eventId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the eventses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of eventses
    * @param end the upper bound of the range of eventses (not inclusive)
    * @return the range of eventses
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.helix.tasks.model.Events> getEventses(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getEventses(start, end);
    }

    /**
    * Returns the number of eventses.
    *
    * @return the number of eventses
    * @throws SystemException if a system exception occurred
    */
    public static int getEventsesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getEventsesCount();
    }

    /**
    * Updates the events in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param events the events
    * @return the events that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.helix.tasks.model.Events updateEvents(
        com.helix.tasks.model.Events events)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateEvents(events);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.helix.tasks.model.Events addEvent(long companyId,
        long groupId, long userId, long sourceId, java.lang.String eventUrl,
        java.lang.String eventName, java.lang.String eventTime,
        java.lang.String eventLocation, java.lang.String imageEu,
        java.lang.String relativeLinkToSite, java.lang.String siteUuid,
        com.liferay.portal.service.ServiceContext serviceContext)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addEvent(companyId, groupId, userId, sourceId, eventUrl,
            eventName, eventTime, eventLocation, imageEu, relativeLinkToSite,
            siteUuid, serviceContext);
    }

    public static java.util.List<com.helix.tasks.model.Events> findBySourceId(
        long sourceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().findBySourceId(sourceId);
    }

    public static void clearService() {
        _service = null;
    }

    public static EventsLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    EventsLocalService.class.getName());

            if (invokableLocalService instanceof EventsLocalService) {
                _service = (EventsLocalService) invokableLocalService;
            } else {
                _service = new EventsLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(EventsLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(EventsLocalService service) {
    }
}
