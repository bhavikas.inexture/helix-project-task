package com.helix.tasks.service.messaging;

import com.helix.tasks.service.ClpSerializer;
import com.helix.tasks.service.EventsLocalServiceUtil;
import com.helix.tasks.service.SourceLocalServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            EventsLocalServiceUtil.clearService();

            SourceLocalServiceUtil.clearService();
        }
    }
}
