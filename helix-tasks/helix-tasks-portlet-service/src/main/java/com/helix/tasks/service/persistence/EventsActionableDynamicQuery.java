package com.helix.tasks.service.persistence;

import com.helix.tasks.model.Events;
import com.helix.tasks.service.EventsLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class EventsActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public EventsActionableDynamicQuery() throws SystemException {
        setBaseLocalService(EventsLocalServiceUtil.getService());
        setClass(Events.class);

        setClassLoader(com.helix.tasks.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("eventId");
    }
}
