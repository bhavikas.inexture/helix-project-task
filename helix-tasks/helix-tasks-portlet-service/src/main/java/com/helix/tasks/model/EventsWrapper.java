package com.helix.tasks.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Events}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Events
 * @generated
 */
public class EventsWrapper implements Events, ModelWrapper<Events> {
    private Events _events;

    public EventsWrapper(Events events) {
        _events = events;
    }

    @Override
    public Class<?> getModelClass() {
        return Events.class;
    }

    @Override
    public String getModelClassName() {
        return Events.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("eventId", getEventId());
        attributes.put("sourceId", getSourceId());
        attributes.put("groupId", getGroupId());
        attributes.put("companyId", getCompanyId());
        attributes.put("userId", getUserId());
        attributes.put("createDate", getCreateDate());
        attributes.put("modifiedDate", getModifiedDate());
        attributes.put("url", getUrl());
        attributes.put("name", getName());
        attributes.put("time", getTime());
        attributes.put("location", getLocation());
        attributes.put("imageEu", getImageEu());
        attributes.put("relativeLinkToSite", getRelativeLinkToSite());
        attributes.put("siteUuid", getSiteUuid());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long eventId = (Long) attributes.get("eventId");

        if (eventId != null) {
            setEventId(eventId);
        }

        Long sourceId = (Long) attributes.get("sourceId");

        if (sourceId != null) {
            setSourceId(sourceId);
        }

        Long groupId = (Long) attributes.get("groupId");

        if (groupId != null) {
            setGroupId(groupId);
        }

        Long companyId = (Long) attributes.get("companyId");

        if (companyId != null) {
            setCompanyId(companyId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date createDate = (Date) attributes.get("createDate");

        if (createDate != null) {
            setCreateDate(createDate);
        }

        Date modifiedDate = (Date) attributes.get("modifiedDate");

        if (modifiedDate != null) {
            setModifiedDate(modifiedDate);
        }

        String url = (String) attributes.get("url");

        if (url != null) {
            setUrl(url);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String time = (String) attributes.get("time");

        if (time != null) {
            setTime(time);
        }

        String location = (String) attributes.get("location");

        if (location != null) {
            setLocation(location);
        }

        String imageEu = (String) attributes.get("imageEu");

        if (imageEu != null) {
            setImageEu(imageEu);
        }

        String relativeLinkToSite = (String) attributes.get(
                "relativeLinkToSite");

        if (relativeLinkToSite != null) {
            setRelativeLinkToSite(relativeLinkToSite);
        }

        String siteUuid = (String) attributes.get("siteUuid");

        if (siteUuid != null) {
            setSiteUuid(siteUuid);
        }
    }

    /**
    * Returns the primary key of this events.
    *
    * @return the primary key of this events
    */
    @Override
    public long getPrimaryKey() {
        return _events.getPrimaryKey();
    }

    /**
    * Sets the primary key of this events.
    *
    * @param primaryKey the primary key of this events
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _events.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the event ID of this events.
    *
    * @return the event ID of this events
    */
    @Override
    public long getEventId() {
        return _events.getEventId();
    }

    /**
    * Sets the event ID of this events.
    *
    * @param eventId the event ID of this events
    */
    @Override
    public void setEventId(long eventId) {
        _events.setEventId(eventId);
    }

    /**
    * Returns the source ID of this events.
    *
    * @return the source ID of this events
    */
    @Override
    public long getSourceId() {
        return _events.getSourceId();
    }

    /**
    * Sets the source ID of this events.
    *
    * @param sourceId the source ID of this events
    */
    @Override
    public void setSourceId(long sourceId) {
        _events.setSourceId(sourceId);
    }

    /**
    * Returns the group ID of this events.
    *
    * @return the group ID of this events
    */
    @Override
    public long getGroupId() {
        return _events.getGroupId();
    }

    /**
    * Sets the group ID of this events.
    *
    * @param groupId the group ID of this events
    */
    @Override
    public void setGroupId(long groupId) {
        _events.setGroupId(groupId);
    }

    /**
    * Returns the company ID of this events.
    *
    * @return the company ID of this events
    */
    @Override
    public long getCompanyId() {
        return _events.getCompanyId();
    }

    /**
    * Sets the company ID of this events.
    *
    * @param companyId the company ID of this events
    */
    @Override
    public void setCompanyId(long companyId) {
        _events.setCompanyId(companyId);
    }

    /**
    * Returns the user ID of this events.
    *
    * @return the user ID of this events
    */
    @Override
    public long getUserId() {
        return _events.getUserId();
    }

    /**
    * Sets the user ID of this events.
    *
    * @param userId the user ID of this events
    */
    @Override
    public void setUserId(long userId) {
        _events.setUserId(userId);
    }

    /**
    * Returns the user uuid of this events.
    *
    * @return the user uuid of this events
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _events.getUserUuid();
    }

    /**
    * Sets the user uuid of this events.
    *
    * @param userUuid the user uuid of this events
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _events.setUserUuid(userUuid);
    }

    /**
    * Returns the create date of this events.
    *
    * @return the create date of this events
    */
    @Override
    public java.util.Date getCreateDate() {
        return _events.getCreateDate();
    }

    /**
    * Sets the create date of this events.
    *
    * @param createDate the create date of this events
    */
    @Override
    public void setCreateDate(java.util.Date createDate) {
        _events.setCreateDate(createDate);
    }

    /**
    * Returns the modified date of this events.
    *
    * @return the modified date of this events
    */
    @Override
    public java.util.Date getModifiedDate() {
        return _events.getModifiedDate();
    }

    /**
    * Sets the modified date of this events.
    *
    * @param modifiedDate the modified date of this events
    */
    @Override
    public void setModifiedDate(java.util.Date modifiedDate) {
        _events.setModifiedDate(modifiedDate);
    }

    /**
    * Returns the url of this events.
    *
    * @return the url of this events
    */
    @Override
    public java.lang.String getUrl() {
        return _events.getUrl();
    }

    /**
    * Sets the url of this events.
    *
    * @param url the url of this events
    */
    @Override
    public void setUrl(java.lang.String url) {
        _events.setUrl(url);
    }

    /**
    * Returns the name of this events.
    *
    * @return the name of this events
    */
    @Override
    public java.lang.String getName() {
        return _events.getName();
    }

    /**
    * Sets the name of this events.
    *
    * @param name the name of this events
    */
    @Override
    public void setName(java.lang.String name) {
        _events.setName(name);
    }

    /**
    * Returns the time of this events.
    *
    * @return the time of this events
    */
    @Override
    public java.lang.String getTime() {
        return _events.getTime();
    }

    /**
    * Sets the time of this events.
    *
    * @param time the time of this events
    */
    @Override
    public void setTime(java.lang.String time) {
        _events.setTime(time);
    }

    /**
    * Returns the location of this events.
    *
    * @return the location of this events
    */
    @Override
    public java.lang.String getLocation() {
        return _events.getLocation();
    }

    /**
    * Sets the location of this events.
    *
    * @param location the location of this events
    */
    @Override
    public void setLocation(java.lang.String location) {
        _events.setLocation(location);
    }

    /**
    * Returns the image eu of this events.
    *
    * @return the image eu of this events
    */
    @Override
    public java.lang.String getImageEu() {
        return _events.getImageEu();
    }

    /**
    * Sets the image eu of this events.
    *
    * @param imageEu the image eu of this events
    */
    @Override
    public void setImageEu(java.lang.String imageEu) {
        _events.setImageEu(imageEu);
    }

    /**
    * Returns the relative link to site of this events.
    *
    * @return the relative link to site of this events
    */
    @Override
    public java.lang.String getRelativeLinkToSite() {
        return _events.getRelativeLinkToSite();
    }

    /**
    * Sets the relative link to site of this events.
    *
    * @param relativeLinkToSite the relative link to site of this events
    */
    @Override
    public void setRelativeLinkToSite(java.lang.String relativeLinkToSite) {
        _events.setRelativeLinkToSite(relativeLinkToSite);
    }

    /**
    * Returns the site uuid of this events.
    *
    * @return the site uuid of this events
    */
    @Override
    public java.lang.String getSiteUuid() {
        return _events.getSiteUuid();
    }

    /**
    * Sets the site uuid of this events.
    *
    * @param siteUuid the site uuid of this events
    */
    @Override
    public void setSiteUuid(java.lang.String siteUuid) {
        _events.setSiteUuid(siteUuid);
    }

    @Override
    public boolean isNew() {
        return _events.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _events.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _events.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _events.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _events.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _events.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _events.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _events.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _events.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _events.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _events.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new EventsWrapper((Events) _events.clone());
    }

    @Override
    public int compareTo(com.helix.tasks.model.Events events) {
        return _events.compareTo(events);
    }

    @Override
    public int hashCode() {
        return _events.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.helix.tasks.model.Events> toCacheModel() {
        return _events.toCacheModel();
    }

    @Override
    public com.helix.tasks.model.Events toEscapedModel() {
        return new EventsWrapper(_events.toEscapedModel());
    }

    @Override
    public com.helix.tasks.model.Events toUnescapedModel() {
        return new EventsWrapper(_events.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _events.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _events.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _events.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EventsWrapper)) {
            return false;
        }

        EventsWrapper eventsWrapper = (EventsWrapper) obj;

        if (Validator.equals(_events, eventsWrapper._events)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Events getWrappedEvents() {
        return _events;
    }

    @Override
    public Events getWrappedModel() {
        return _events;
    }

    @Override
    public void resetOriginalValues() {
        _events.resetOriginalValues();
    }
}
