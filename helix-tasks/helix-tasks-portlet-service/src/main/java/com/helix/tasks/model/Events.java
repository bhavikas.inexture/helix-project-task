package com.helix.tasks.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Events service. Represents a row in the &quot;Helix_Events&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see EventsModel
 * @see com.helix.tasks.model.impl.EventsImpl
 * @see com.helix.tasks.model.impl.EventsModelImpl
 * @generated
 */
public interface Events extends EventsModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.helix.tasks.model.impl.EventsImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
