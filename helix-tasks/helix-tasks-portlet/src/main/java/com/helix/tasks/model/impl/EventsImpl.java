package com.helix.tasks.model.impl;

/**
 * The extended model implementation for the Events service. Represents a row in the &quot;Helix_Events&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.helix.tasks.model.Events} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class EventsImpl extends EventsBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a events model instance should use the {@link com.helix.tasks.model.Events} interface instead.
     */
    public EventsImpl() {
    }
}
