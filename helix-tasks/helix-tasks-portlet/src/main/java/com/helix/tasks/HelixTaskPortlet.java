package com.helix.tasks;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.helix.tasks.background.task.HelixBackgroundTaskExecutor;
import com.helix.tasks.model.Events;
import com.helix.tasks.model.Source;
import com.helix.tasks.service.EventsLocalServiceUtil;
import com.helix.tasks.service.SourceLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.BackgroundTask;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.service.BackgroundTaskLocalServiceUtil;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.PortletLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

public class HelixTaskPortlet extends MVCPortlet{
	Log log = LogFactoryUtil.getLog(HelixTaskPortlet.class.getClass());
	
	/**
	 * get Source and Events data to display on the page
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		// get source and its events to display on the page
		List<Map<String, String>> res = new ArrayList<Map<String, String>>();
		try {
			List<Source> sources = SourceLocalServiceUtil.getSources(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			if(Validator.isNotNull(sources) && !sources.isEmpty()) {
				sources.stream().forEach(source -> {
					try {
						List<Events> events = EventsLocalServiceUtil.findBySourceId(source.getSourceId());
						
						if(Validator.isNotNull(events) && !events.isEmpty()) {
							events.stream().forEach(event -> {
								Map<String, String> data = new TreeMap<String, String>();
								
								data.put("organizer", source.getOrganizer());
								data.put("city", source.getCity());
								data.put("country", source.getCountry());
								data.put("event_name", event.getName());
								data.put("location", event.getLocation());
								data.put("date_time", event.getTime());
								data.put("relative_link", themeDisplay.getPathFriendlyURLPublic() + event.getRelativeLinkToSite());

								res.add(data);
							});
						} else {
							Map<String, String> data = new TreeMap<String, String>();
							
							data.put("organizer", source.getOrganizer());
							data.put("city", source.getCity());
							data.put("country", source.getCountry());
							data.put("event_name", StringPool.BLANK);
							data.put("location", StringPool.BLANK);
							data.put("date_time", StringPool.BLANK);
							data.put("relative_link", StringPool.BLANK);

							res.add(data);
						}
					} catch (SystemException e) {
						log.error("Exception while fetching all events for source: "+ source.getSourceId(), e);
					}
				});
			}
		} catch (SystemException e) {
			log.error("Exception while fetching all sources.", e);
		}
		
		request.setAttribute("response", res);
		super.render(request, response);
	}
	
	/**
	 * handle form action request to read uploaded file and create background task to process on file data
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @throws PortletException
	 * @throws IOException
	 * @throws PortalException
	 * @throws SystemException
	 */
	public void uploadFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException, PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
		
		// get uploaded file
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		log.info("fileName: "+uploadRequest.getFileName("file")); // print uploaded filename
		File file = uploadRequest.getFile("file"); // get the file in File object
		
		// read the CSV file using CSVReader, read all the rows from the file
		CSVReader csvReader = new CSVReader(new FileReader(file));
		List<String[]> rows = csvReader.readAll(); //
		
		log.info("rows size: "+rows.size());
		
		// validate if CSV file is empty or includes valid header row
		if(Validator.isNull(rows) || rows.isEmpty() || rows.size() <= 1) { 
			log.info("No records in uploaded CSV file or no valid header provided!");
			// if any exception accurs while adding background task, add custom error-message to display on the JSP page
			SessionErrors.add(actionRequest, "empty-invalid-csv-file");
			// to hide default error message
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		} else {
			// if header exists
			if(HelixTaskConstants.CSV_FILE_HEADERS.equalsIgnoreCase(Arrays.toString(rows.get(0)))) {
				// reader header row of CSV file and remove it from list.
				String[] headerRow = rows.get(0);
				rows.remove(0);
				log.info(headerRow[0] + " :: " + headerRow[1]);
			}
			
			// generate background task context map to send to data in background process
			Map<String, Serializable> taskContextMap = new TreeMap<>();
			taskContextMap.put("rows", (Serializable) rows);
			taskContextMap.put("serviceContext", serviceContext);
			
			// get portlet object with help of Portlet Id and generate string array having portlet context name
			Portlet portlet = PortletLocalServiceUtil.getPortletById(actionRequest.getAttribute(WebKeys.PORTLET_ID).toString());
			String servletContextNames[] = new String[] {portlet.getContextName()};
			
			try {
				// add a background task to do processing on the file rows data
				BackgroundTask backgroundTask = BackgroundTaskLocalServiceUtil.addBackgroundTask(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(),
						"file_processing_task", servletContextNames, HelixBackgroundTaskExecutor.class, taskContextMap, serviceContext);
			
				log.info("backgroundTaskId: "+backgroundTask.getBackgroundTaskId());
			} catch (PortalException | SystemException e) {
				// if any exception accurs while adding background task, add custom error-message to display on the JSP page
				SessionErrors.add(actionRequest, "error-message");
				// to hide default error message
				SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				
				log.error("Exception while adding/executing background task", e);
			}
			
			// add custom success message
			SessionMessages.add(actionRequest, "file-upload-request-completed");
		}
	}
	
	/**
	 * get data from Source and Events tables, generate and download CSV file
	 */
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		// get source and its events to write to the CSV file
		List<Map<String, String>> response = new ArrayList<Map<String, String>>();
		try {
			List<Source> sources = SourceLocalServiceUtil.getSources(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			if(Validator.isNotNull(sources) && !sources.isEmpty()) {
				sources.stream().forEach(source -> {
					try {
						List<Events> events = EventsLocalServiceUtil.findBySourceId(source.getSourceId());
						
						if(Validator.isNotNull(events) && !events.isEmpty()) {
							events.stream().forEach(event -> {
								Map<String, String> data = new TreeMap<String, String>();
								
								data.put("event_source", source.getEventSource());
								data.put("event_source_link", source.getEventSourceLink());
								data.put("organizer", source.getOrganizer());
								data.put("city", source.getCity());
								data.put("country", source.getCountry());
								data.put("event_name", event.getName());
								data.put("event_url", event.getUrl());
								data.put("location", event.getLocation());
								data.put("date_time", event.getTime());
								data.put("relative_link_to_liferay_site", themeDisplay.getPathFriendlyURLPublic() + event.getRelativeLinkToSite());
								data.put("uuid", event.getSiteUuid());
								
								response.add(data);
							});
						} else {
							Map<String, String> data = new TreeMap<String, String>();
							
							data.put("event_source", source.getEventSource());
							data.put("event_source_link", source.getEventSourceLink());
							data.put("organizer", source.getOrganizer());
							data.put("city", source.getCity());
							data.put("country", source.getCountry());
							data.put("event_name", StringPool.BLANK);
							data.put("event_url", StringPool.BLANK);
							data.put("location", StringPool.BLANK);
							data.put("date_time", StringPool.BLANK);
							data.put("relative_link_to_liferay_site", StringPool.BLANK);
							data.put("uuid", StringPool.BLANK);
							
							response.add(data);
						}
					} catch (SystemException e) {
						log.error("Exception while fetching all events for source: "+ source.getSourceId(), e);
					}
				});
			}
		} catch (SystemException e) {
			log.error("Exception while fetching all sources.", e);
		}
		
		List<String[]> records = new ArrayList<String[]>();
		records.add(new String[] {
				"event_source", 
				"event_source_link", 
				"organizer", 
				"event_name", 
				"event_url", 
				"city",
				"country",
				"location", 
				"date_time", 
				"relative_link_to_liferay_site", 
				"uuid"});
		
		if(!response.isEmpty()) {
			response.stream().forEach(action -> {
				records.add(new String[] {
						action.get("event_source"),
						action.get("event_source_link"), 
						action.get("organizer"), 
						action.get("event_name"), 
						action.get("event_url"), 
						action.get("city"), 
						action.get("country"), 
						action.get("location"), 
						action.get("date_time"), 
						action.get("relative_link_to_liferay_site"), 
						action.get("uuid")
				});
			});
		}
			
		StringWriter writer = new StringWriter();
		
		CSVWriter csvWriter = new CSVWriter(writer);
		csvWriter.writeAll(records);
		csvWriter.close();
		
		resourceResponse.setContentType("text/csv");
		resourceResponse.addProperty("Content-disposition", "atachment; filename=sample.csv");
		
		OutputStream out = resourceResponse.getPortletOutputStream();
		out.write(writer.toString().getBytes("UTF-8"));
	}
	
	/**
	 * customNotificationAction
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 */
	public void customNotificationAction(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
	}
}
