package com.helix.tasks.background.task;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.helix.tasks.HelixTaskConstants;
import com.helix.tasks.HelixTasksUtil;
import com.helix.tasks.NoSuchSourceException;
import com.helix.tasks.model.Events;
import com.helix.tasks.model.Source;
import com.helix.tasks.service.EventsLocalServiceUtil;
import com.helix.tasks.service.SourceLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.PortalClassInvoker;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BackgroundTask;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.LayoutSetPrototypeLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.sites.util.SitesUtil;
import com.liferay.portal.model.LayoutSetPrototype;

public class HelixBackgroundTaskExecutor extends BaseBackgroundTaskExecutor{
	private static final Log log = LogFactoryUtil.getLog(HelixBackgroundTaskExecutor.class.getName());
	
	@Override
	public BackgroundTaskResult execute(BackgroundTask backgroundTask) throws Exception {
		log.info("Executing background task");
		
		// get the background task context map
		Map<String, Serializable> taskContextMap = backgroundTask.getTaskContextMap();
		List<String[]> rows = (List<String[]>) taskContextMap.get("rows");
		ServiceContext serviceContext = (ServiceContext) taskContextMap.get("serviceContext");
		
		// notification JSON
		JSONObject payloadJSON = JSONFactoryUtil.createJSONObject();
		payloadJSON.put("subject", "CSV Upload Task");
		
		// validate if rows list is not null/empty
		if(Validator.isNotNull(rows) && !rows.isEmpty()) {
			rows.stream().forEach(action -> {
				boolean isSourceAlreadyExists = false;
				String eventSource = action[0];
				String eventSourceLink = action[1];
				
				log.info("event_source: "+eventSource);
				log.info("event_source_link: "+eventSourceLink);
				
				try {
					// check if source already exists in database
					Source source = SourceLocalServiceUtil.findByEventSourceLink(eventSourceLink);
					
					// if source already exists, then skip the below steps
					if(Validator.isNotNull(source)) {
						isSourceAlreadyExists = true;
						log.info("Source ("+eventSourceLink+") already exists. SourceId:"+source.getSourceId());
					}
				} catch (NoSuchSourceException | SystemException ex) {
					log.error("Exception while getting source by eventSourceLink "+ eventSourceLink);
				}
				
				if(!isSourceAlreadyExists) {
					// create document object and connect with url to read HTML page
					Document doc = null;
					try {
						doc = Jsoup.connect(eventSourceLink+"events/").get();
					} catch (IOException e) {
						log.error("Exception while parcing HTML from the url: "+eventSourceLink);
						log.info(e.getMessage());
					}
		
					// if document is not null and document available, fetch required data
					if(Validator.isNotNull(doc)) {
						String imageEsl = doc.getElementsByClass("groupHomeHeader-banner").attr("style");
						String organizer = doc.getElementsByClass("groupHomeHeader-groupNameLink").text();
						String organizer_city = doc.selectFirst(".organizer-city .groupHomeHeaderInfo-cityLink").text();
						String city = "";
						String country = "";
						
						// Separate citty and country name
						if(!organizer_city.isEmpty()) {
							city = organizer_city.split(",")[0].trim();
							country = organizer_city.split(",")[1].trim();
						}
						
						if(!imageEsl.isEmpty()) {
							// separate image url from the extra css string
							if(imageEsl.indexOf("http") == -1) {
								imageEsl = imageEsl.replaceFirst("background", "").replaceFirst("-", "").replaceFirst("image", "").replaceFirst(":", "").replaceFirst("url", "");
								imageEsl = imageEsl.substring(1, imageEsl.length()-1);
							} else {
								imageEsl = imageEsl.substring(imageEsl.indexOf("http"), imageEsl.length()-1);
							}
							
							DLFileEntry dlFileEntry = HelixTasksUtil.saveImage(serviceContext.getUserId(), serviceContext.getScopeGroupId(), imageEsl);
							if(Validator.isNotNull(dlFileEntry)) {
								imageEsl = dlFileEntry.getUuid();
							}
						}
						
						try {
							// add source into the database if not already exists
							Source source = SourceLocalServiceUtil.addSource(serviceContext.getCompanyId(), serviceContext.getScopeGroupId(), serviceContext.getUserId(), eventSource, eventSourceLink, imageEsl, organizer, city, country, serviceContext);
							
							// fetch all the upcoming events data
							Elements events = doc.select("ul.eventList-list").select(".list-item");
							if(Validator.isNotNull(events) && !events.isEmpty()) {
								events.stream().forEach(event -> {
									String eventTime = event.select("time").text();
									String eventNameWithoutFilter = event.selectFirst("a.eventCard--link").text();
									String eventName = HelixTasksUtil.eventName(eventNameWithoutFilter);
									String eventUrl = event.selectFirst("a.eventCard--link").absUrl("href");
									String imageEu = "";
									String location = "";
									
									
									// create document object and connect with event url to read HTML page
									Document childDoc = null;
									try {
										childDoc = Jsoup.connect(eventUrl).get();
									} catch (IOException e) {
										log.error("Exception while parcing HTML from the url: "+eventUrl, e);
									}
									
									if(Validator.isNotNull(childDoc)) {
										imageEu = childDoc.select(".photoCarousel-photoContainer").attr("style");
										location = childDoc.select(".venueDisplay.venueDisplay-venue .venueDisplay-venue-address").text();
										
										if(!imageEu.isEmpty()) {
											// separate image url from the extra css string
											if(imageEu.indexOf("http") == -1) {
												imageEu = imageEu.replaceFirst("background", "").replaceFirst("-", "").replaceFirst("image", "").replaceFirst(":", "").replaceFirst("url", "");
												imageEu = imageEu.substring(1, imageEu.length()-1);
											} else {
												imageEu = imageEu.substring(imageEu.indexOf("http"), imageEu.length()-1);
											}
											
											DLFileEntry dlFileEntry = HelixTasksUtil.saveImage(serviceContext.getUserId(), serviceContext.getScopeGroupId(), imageEu);
											if(Validator.isNotNull(dlFileEntry)) {
												imageEu = dlFileEntry.getUuid();
											}
										}
									}
									
									// create group(site) of the event
									Group group = null;
									try {
										group = GroupLocalServiceUtil.addGroup(serviceContext.getUserId(), GroupConstants.DEFAULT_PARENT_GROUP_ID, Group.class.getName(), CounterLocalServiceUtil.increment(), GroupConstants.DEFAULT_LIVE_GROUP_ID, eventName, eventName, GroupConstants.TYPE_SITE_OPEN, true, GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION, StringPool.BLANK, true, true, serviceContext);
										
										if(!source.getImageEsl().isEmpty()) {
											HelixTasksUtil.copyImage(serviceContext.getScopeGroupId(), source.getImageEsl(), group);
										}
										
										if(!imageEu.isEmpty()) {
											DLFileEntry dlFileEntry = HelixTasksUtil.copyImage(serviceContext.getScopeGroupId(), imageEu, group);
											imageEu = dlFileEntry.getUuid();
										}
										
										try {
											// add events along with group info into the database for the source
											Events eventRecord = EventsLocalServiceUtil.addEvent(serviceContext.getCompanyId(), serviceContext.getScopeGroupId(), serviceContext.getUserId(), source.getSourceId(), eventUrl, eventName, eventTime, location, imageEu, group.getFriendlyURL(), group.getUuid(), serviceContext);
											if(Validator.isNotNull(eventRecord)) {
//												log.info("Event(eventId:"+eventRecord.getEventId()+") added successfully for the source (sourceId:"+source.getSourceId()+")");
											}
										} catch (PortalException | SystemException ex) {
											log.error("Exception while adding event("+eventName+") to database for source "+source.getSourceId());
										}
										
									} catch (Exception e) {
										log.info("===========================Exception while creating/getting a Group named====================================== "+eventName);
										log.error("Exception while creating/getting a Group named "+ eventName);
									}
									
									// once group is created, update group -> assign mentioned site templates to the public and private pages of the group
									if(Validator.isNotNull(group)) {
										try {
											// get all the layouts of the company
											List<LayoutSetPrototype> layoutSets = LayoutSetPrototypeLocalServiceUtil.getLayoutSetPrototypes(serviceContext.getCompanyId());
											if(Validator.isNotNull(layoutSets) && !layoutSets.isEmpty()) {
												// get "Public Pages" site template
												LayoutSetPrototype publicLayoutSetPrototype = layoutSets.stream().filter(predicate -> HelixTaskConstants.CUSTOM_PUBLIC_PAGES_TEMPLATE.equalsIgnoreCase(predicate.getName(Locale.US))
												).findAny().orElse(null);
												
												// get "Private Pages" site template
												LayoutSetPrototype privateLayoutSetPrototype = layoutSets.stream().filter(predicate -> HelixTaskConstants.CUSTOM_PRIVATE_PAGES_TEMPLATE.equalsIgnoreCase(predicate.getName(Locale.US))
														).findAny().orElse(null);
												
												// once, both the templates are available, update to assign templates to public and private pages of the created event group
												if(Validator.isNotNull(publicLayoutSetPrototype) && Validator.isNotNull(privateLayoutSetPrototype)) {
													log.info("PortalClassInvoker:: _updateLayoutSetPrototypesMethodKey");
													PortalClassInvoker.invoke(true, _updateLayoutSetPrototypesMethodKey, 
															group, publicLayoutSetPrototype.getLayoutSetPrototypeId(), privateLayoutSetPrototype.getLayoutSetPrototypeId(), 
															true, true);
													
													// get public and private layoutset of the group
													LayoutSet publicLayoutSet = LayoutSetLocalServiceUtil.getLayoutSet(group.getGroupId(), false);
													LayoutSet privateLayoutSet = LayoutSetLocalServiceUtil.getLayoutSet(group.getGroupId(), true);
													
													// if both are not null
													if(Validator.isNotNull(publicLayoutSet) && Validator.isNotNull(privateLayoutSet)) {
														log.info("Public PortalClassInvoker:: _mergeLayoutSetPrototypeLayoutsMethodKey");
														PortalClassInvoker.invoke(true,
																_mergeLayoutSetPrototypeLayoutsMethodKey,
																group, publicLayoutSet);
														
														log.info("Private PortalClassInvoker:: _mergeLayoutSetPrototypeLayoutsMethodKey");
														PortalClassInvoker.invoke(true,
																_mergeLayoutSetPrototypeLayoutsMethodKey,
																group, privateLayoutSet);
													}
												}
											}
										} catch (Exception e) {
											log.error("Exception while PortalClassInvoker for group "+ eventName, e);
											payloadJSON.put("message", "Something went wrong while assigning site templates to the site of event "+ eventName + "Please try again.");
											
											// if fails, send notification to inform
											try {
												UserNotificationEventLocalServiceUtil.addUserNotificationEvent(serviceContext.getUserId(), 
														HelixTaskConstants.PORTLET_ID, (new Date()).getTime(), serviceContext.getUserId(),
														payloadJSON.toString(), false, serviceContext);
											} catch (PortalException | SystemException ex) {
												log.error("Exception while adding notification for site template assigning fail for group of event::"+eventName, ex);
											}
										}
									}
								});
							}
						} catch (PortalException | SystemException ex) {
							log.error("Exception while adding source("+eventSourceLink+") data into the database.", ex);
						}	
					}
				}
			});
		}
		
		log.info("Background task executed!");
		log.info("Adding notification for background task completetion...");
		
		// success message to send notification on completion of this background task
		payloadJSON.put("message", "CSV file processed successfully.");
		
		UserNotificationEventLocalServiceUtil.addUserNotificationEvent(serviceContext.getUserId(), 
				HelixTaskConstants.PORTLET_ID, (new Date()).getTime(), serviceContext.getUserId(),
				payloadJSON.toString(), false, serviceContext);	
		
		BackgroundTaskResult backgroundTaskResult = new BackgroundTaskResult(
			      BackgroundTaskConstants.STATUS_SUCCESSFUL);
		backgroundTaskResult.setStatusMessage("CSV file processed successfully.");
		
		return backgroundTaskResult;
	}

	private static MethodKey _mergeLayoutSetPrototypeLayoutsMethodKey = new MethodKey(
			SitesUtil.class, "mergeLayoutSetProtypeLayouts", Group.class, LayoutSet.class);
	
	private static MethodKey _updateLayoutSetPrototypesMethodKey = new MethodKey(
			SitesUtil.class, "updateLayoutSetPrototypesLinks", Group.class,
			long.class, long.class, boolean.class, boolean.class);
}
