package com.helix.tasks.service.persistence;

import com.helix.tasks.NoSuchSourceException;
import com.helix.tasks.model.Source;
import com.helix.tasks.model.impl.SourceImpl;
import com.helix.tasks.model.impl.SourceModelImpl;
import com.helix.tasks.service.persistence.SourcePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the source service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SourcePersistence
 * @see SourceUtil
 * @generated
 */
public class SourcePersistenceImpl extends BasePersistenceImpl<Source>
    implements SourcePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link SourceUtil} to access the source persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = SourceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceModelImpl.FINDER_CACHE_ENABLED, SourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceModelImpl.FINDER_CACHE_ENABLED, SourceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_EVENTSOURCELINK = new FinderPath(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceModelImpl.FINDER_CACHE_ENABLED, SourceImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByEventSourceLink",
            new String[] { String.class.getName() },
            SourceModelImpl.EVENTSOURCELINK_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_EVENTSOURCELINK = new FinderPath(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByEventSourceLink", new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_1 =
        "source.eventSourceLink IS NULL";
    private static final String _FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_2 =
        "source.eventSourceLink = ?";
    private static final String _FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_3 =
        "(source.eventSourceLink IS NULL OR source.eventSourceLink = '')";
    private static final String _SQL_SELECT_SOURCE = "SELECT source FROM Source source";
    private static final String _SQL_SELECT_SOURCE_WHERE = "SELECT source FROM Source source WHERE ";
    private static final String _SQL_COUNT_SOURCE = "SELECT COUNT(source) FROM Source source";
    private static final String _SQL_COUNT_SOURCE_WHERE = "SELECT COUNT(source) FROM Source source WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "source.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Source exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Source exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(SourcePersistenceImpl.class);
    private static Source _nullSource = new SourceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Source> toCacheModel() {
                return _nullSourceCacheModel;
            }
        };

    private static CacheModel<Source> _nullSourceCacheModel = new CacheModel<Source>() {
            @Override
            public Source toEntityModel() {
                return _nullSource;
            }
        };

    public SourcePersistenceImpl() {
        setModelClass(Source.class);
    }

    /**
     * Returns the source where eventSourceLink = &#63; or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
     *
     * @param eventSourceLink the event source link
     * @return the matching source
     * @throws com.helix.tasks.NoSuchSourceException if a matching source could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source findByEventSourceLink(String eventSourceLink)
        throws NoSuchSourceException, SystemException {
        Source source = fetchByEventSourceLink(eventSourceLink);

        if (source == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("eventSourceLink=");
            msg.append(eventSourceLink);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchSourceException(msg.toString());
        }

        return source;
    }

    /**
     * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param eventSourceLink the event source link
     * @return the matching source, or <code>null</code> if a matching source could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source fetchByEventSourceLink(String eventSourceLink)
        throws SystemException {
        return fetchByEventSourceLink(eventSourceLink, true);
    }

    /**
     * Returns the source where eventSourceLink = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param eventSourceLink the event source link
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching source, or <code>null</code> if a matching source could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source fetchByEventSourceLink(String eventSourceLink,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { eventSourceLink };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                    finderArgs, this);
        }

        if (result instanceof Source) {
            Source source = (Source) result;

            if (!Validator.equals(eventSourceLink, source.getEventSourceLink())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_SOURCE_WHERE);

            boolean bindEventSourceLink = false;

            if (eventSourceLink == null) {
                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_1);
            } else if (eventSourceLink.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_3);
            } else {
                bindEventSourceLink = true;

                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindEventSourceLink) {
                    qPos.add(eventSourceLink);
                }

                List<Source> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "SourcePersistenceImpl.fetchByEventSourceLink(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Source source = list.get(0);

                    result = source;

                    cacheResult(source);

                    if ((source.getEventSourceLink() == null) ||
                            !source.getEventSourceLink().equals(eventSourceLink)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                            finderArgs, source);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Source) result;
        }
    }

    /**
     * Removes the source where eventSourceLink = &#63; from the database.
     *
     * @param eventSourceLink the event source link
     * @return the source that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source removeByEventSourceLink(String eventSourceLink)
        throws NoSuchSourceException, SystemException {
        Source source = findByEventSourceLink(eventSourceLink);

        return remove(source);
    }

    /**
     * Returns the number of sources where eventSourceLink = &#63;.
     *
     * @param eventSourceLink the event source link
     * @return the number of matching sources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByEventSourceLink(String eventSourceLink)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_EVENTSOURCELINK;

        Object[] finderArgs = new Object[] { eventSourceLink };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_SOURCE_WHERE);

            boolean bindEventSourceLink = false;

            if (eventSourceLink == null) {
                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_1);
            } else if (eventSourceLink.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_3);
            } else {
                bindEventSourceLink = true;

                query.append(_FINDER_COLUMN_EVENTSOURCELINK_EVENTSOURCELINK_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindEventSourceLink) {
                    qPos.add(eventSourceLink);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the source in the entity cache if it is enabled.
     *
     * @param source the source
     */
    @Override
    public void cacheResult(Source source) {
        EntityCacheUtil.putResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceImpl.class, source.getPrimaryKey(), source);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
            new Object[] { source.getEventSourceLink() }, source);

        source.resetOriginalValues();
    }

    /**
     * Caches the sources in the entity cache if it is enabled.
     *
     * @param sources the sources
     */
    @Override
    public void cacheResult(List<Source> sources) {
        for (Source source : sources) {
            if (EntityCacheUtil.getResult(
                        SourceModelImpl.ENTITY_CACHE_ENABLED, SourceImpl.class,
                        source.getPrimaryKey()) == null) {
                cacheResult(source);
            } else {
                source.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all sources.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(SourceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(SourceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the source.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Source source) {
        EntityCacheUtil.removeResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceImpl.class, source.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(source);
    }

    @Override
    public void clearCache(List<Source> sources) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Source source : sources) {
            EntityCacheUtil.removeResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
                SourceImpl.class, source.getPrimaryKey());

            clearUniqueFindersCache(source);
        }
    }

    protected void cacheUniqueFindersCache(Source source) {
        if (source.isNew()) {
            Object[] args = new Object[] { source.getEventSourceLink() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EVENTSOURCELINK,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                args, source);
        } else {
            SourceModelImpl sourceModelImpl = (SourceModelImpl) source;

            if ((sourceModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_EVENTSOURCELINK.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { source.getEventSourceLink() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EVENTSOURCELINK,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                    args, source);
            }
        }
    }

    protected void clearUniqueFindersCache(Source source) {
        SourceModelImpl sourceModelImpl = (SourceModelImpl) source;

        Object[] args = new Object[] { source.getEventSourceLink() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EVENTSOURCELINK, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK, args);

        if ((sourceModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_EVENTSOURCELINK.getColumnBitmask()) != 0) {
            args = new Object[] { sourceModelImpl.getOriginalEventSourceLink() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EVENTSOURCELINK,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EVENTSOURCELINK,
                args);
        }
    }

    /**
     * Creates a new source with the primary key. Does not add the source to the database.
     *
     * @param sourceId the primary key for the new source
     * @return the new source
     */
    @Override
    public Source create(long sourceId) {
        Source source = new SourceImpl();

        source.setNew(true);
        source.setPrimaryKey(sourceId);

        return source;
    }

    /**
     * Removes the source with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param sourceId the primary key of the source
     * @return the source that was removed
     * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source remove(long sourceId)
        throws NoSuchSourceException, SystemException {
        return remove((Serializable) sourceId);
    }

    /**
     * Removes the source with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the source
     * @return the source that was removed
     * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source remove(Serializable primaryKey)
        throws NoSuchSourceException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Source source = (Source) session.get(SourceImpl.class, primaryKey);

            if (source == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchSourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(source);
        } catch (NoSuchSourceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Source removeImpl(Source source) throws SystemException {
        source = toUnwrappedModel(source);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(source)) {
                source = (Source) session.get(SourceImpl.class,
                        source.getPrimaryKeyObj());
            }

            if (source != null) {
                session.delete(source);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (source != null) {
            clearCache(source);
        }

        return source;
    }

    @Override
    public Source updateImpl(com.helix.tasks.model.Source source)
        throws SystemException {
        source = toUnwrappedModel(source);

        boolean isNew = source.isNew();

        Session session = null;

        try {
            session = openSession();

            if (source.isNew()) {
                session.save(source);

                source.setNew(false);
            } else {
                session.merge(source);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !SourceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
            SourceImpl.class, source.getPrimaryKey(), source);

        clearUniqueFindersCache(source);
        cacheUniqueFindersCache(source);

        return source;
    }

    protected Source toUnwrappedModel(Source source) {
        if (source instanceof SourceImpl) {
            return source;
        }

        SourceImpl sourceImpl = new SourceImpl();

        sourceImpl.setNew(source.isNew());
        sourceImpl.setPrimaryKey(source.getPrimaryKey());

        sourceImpl.setSourceId(source.getSourceId());
        sourceImpl.setGroupId(source.getGroupId());
        sourceImpl.setCompanyId(source.getCompanyId());
        sourceImpl.setUserId(source.getUserId());
        sourceImpl.setCreateDate(source.getCreateDate());
        sourceImpl.setModifiedDate(source.getModifiedDate());
        sourceImpl.setEventSource(source.getEventSource());
        sourceImpl.setEventSourceLink(source.getEventSourceLink());
        sourceImpl.setImageEsl(source.getImageEsl());
        sourceImpl.setOrganizer(source.getOrganizer());
        sourceImpl.setCity(source.getCity());
        sourceImpl.setCountry(source.getCountry());

        return sourceImpl;
    }

    /**
     * Returns the source with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the source
     * @return the source
     * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source findByPrimaryKey(Serializable primaryKey)
        throws NoSuchSourceException, SystemException {
        Source source = fetchByPrimaryKey(primaryKey);

        if (source == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchSourceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return source;
    }

    /**
     * Returns the source with the primary key or throws a {@link com.helix.tasks.NoSuchSourceException} if it could not be found.
     *
     * @param sourceId the primary key of the source
     * @return the source
     * @throws com.helix.tasks.NoSuchSourceException if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source findByPrimaryKey(long sourceId)
        throws NoSuchSourceException, SystemException {
        return findByPrimaryKey((Serializable) sourceId);
    }

    /**
     * Returns the source with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the source
     * @return the source, or <code>null</code> if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Source source = (Source) EntityCacheUtil.getResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
                SourceImpl.class, primaryKey);

        if (source == _nullSource) {
            return null;
        }

        if (source == null) {
            Session session = null;

            try {
                session = openSession();

                source = (Source) session.get(SourceImpl.class, primaryKey);

                if (source != null) {
                    cacheResult(source);
                } else {
                    EntityCacheUtil.putResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
                        SourceImpl.class, primaryKey, _nullSource);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(SourceModelImpl.ENTITY_CACHE_ENABLED,
                    SourceImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return source;
    }

    /**
     * Returns the source with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param sourceId the primary key of the source
     * @return the source, or <code>null</code> if a source with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Source fetchByPrimaryKey(long sourceId) throws SystemException {
        return fetchByPrimaryKey((Serializable) sourceId);
    }

    /**
     * Returns all the sources.
     *
     * @return the sources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Source> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the sources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of sources
     * @param end the upper bound of the range of sources (not inclusive)
     * @return the range of sources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Source> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the sources.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.SourceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of sources
     * @param end the upper bound of the range of sources (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of sources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Source> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Source> list = (List<Source>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SOURCE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SOURCE;

                if (pagination) {
                    sql = sql.concat(SourceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Source>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Source>(list);
                } else {
                    list = (List<Source>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the sources from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Source source : findAll()) {
            remove(source);
        }
    }

    /**
     * Returns the number of sources.
     *
     * @return the number of sources
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SOURCE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the source persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.helix.tasks.model.Source")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Source>> listenersList = new ArrayList<ModelListener<Source>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Source>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(SourceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
