package com.helix.tasks.service.impl;

import java.util.Date;
import java.util.List;

import com.helix.tasks.model.Events;
import com.helix.tasks.service.base.EventsLocalServiceBaseImpl;
import com.helix.tasks.service.persistence.EventsUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the events local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.helix.tasks.service.EventsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.helix.tasks.service.base.EventsLocalServiceBaseImpl
 * @see com.helix.tasks.service.EventsLocalServiceUtil
 */
public class EventsLocalServiceImpl extends EventsLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.helix.tasks.service.EventsLocalServiceUtil} to access the events local service.
     */
	
	public Events addEvent(long companyId, long groupId, long userId, long sourceId, String eventUrl, String eventName, String eventTime, String eventLocation, String imageEu, String relativeLinkToSite, String siteUuid, ServiceContext serviceContext) throws PortalException, SystemException {
		long eventId = counterLocalService.increment(Events.class.getName());
		
		Events event = eventsPersistence.create(eventId);
		
		event.setCompanyId(companyId);
		event.setGroupId(groupId);
		event.setUserId(userId);
		event.setSourceId(sourceId);
		
		event.setName(eventName);
		event.setUrl(eventUrl);
		event.setTime(eventTime);
		event.setLocation(eventLocation);
		event.setImageEu(imageEu);
		event.setRelativeLinkToSite(relativeLinkToSite);
		event.setSiteUuid(siteUuid);
		
		Date now = new Date();
		event.setCreateDate(serviceContext.getCreateDate(now));
		event.setModifiedDate(serviceContext.getModifiedDate(now));
		
		event = super.addEvents(event);
		
		return event;
	}
	
	public List<Events> findBySourceId(long sourceId) throws SystemException {
		return EventsUtil.findBySourceId(sourceId);
	}
}
