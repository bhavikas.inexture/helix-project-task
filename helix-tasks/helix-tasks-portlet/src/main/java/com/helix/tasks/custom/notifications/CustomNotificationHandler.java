package com.helix.tasks.custom.notifications;

import javax.portlet.ActionRequest;
import javax.portlet.PortletMode;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.WindowState;

import com.helix.tasks.HelixTaskConstants;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.notifications.BaseUserNotificationHandler;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.UserNotificationEvent;
import com.liferay.portal.service.ServiceContext;

public class CustomNotificationHandler extends BaseUserNotificationHandler{
	private static final Log log = LogFactoryUtil.getLog(CustomNotificationHandler.class.getName());
	
	public CustomNotificationHandler() {
		log.info("Executing CustomNotificationHandler constructor - set portlet Id");
		setPortletId(HelixTaskConstants.PORTLET_ID);
	}
	
	@Override
	protected String getBody(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext)
			throws Exception {
		log.info("Executing getBody method");
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		String title = "<strong>" + jsonObject.getString("subject") + "</strong>";
		String bodyText = jsonObject.getString("message");
		String body = StringUtil.replace(getBodyTemplate(), new String[] { "[$TITLE$]", "[$BODY_TEXT$]" }, new String[] { title, bodyText });
		return body;
	}
	
	@Override
	protected String getLink(UserNotificationEvent userNotificationEvent, ServiceContext serviceContext)
			throws Exception {
		log.info("Executing getLink method");
		
		LiferayPortletResponse liferayPortletResponse = serviceContext.getLiferayPortletResponse();		
		 
		PortletURL viewURL = liferayPortletResponse.createActionURL(HelixTaskConstants.PORTLET_ID);
		viewURL.setParameter(ActionRequest.ACTION_NAME, "customNotificationAction");
		viewURL.setParameter("redirect", serviceContext.getLayoutFullURL());
		viewURL.setParameter("userNotificationEventId", String.valueOf(userNotificationEvent.getUserNotificationEventId()));
		viewURL.setWindowState(WindowState.NORMAL);
		
		return viewURL.toString();
	}
	
	protected String getBodyTemplate() throws Exception {
		log.info("Executing getBodyTemplate method");
		
		StringBundler sb = new StringBundler(5);
		sb.append("<div class=\"title\">[$TITLE$]</div><div ");
		sb.append("class=\"body\">[$BODY_TEXT$]</div>");
		return sb.toString();
	}
}
