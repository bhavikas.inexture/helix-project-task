package com.helix.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.poi.util.IOUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntryTypeConstants;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;

public class HelixTasksUtil {
	private static final Log log = LogFactoryUtil.getLog(HelixTasksUtil.class.getName());
	
	public static DLFileEntry saveImage(long userId, long groupId, String imageUrl) {
		log.info("Excecuting saveImage for url: "+imageUrl);
		
		try {
			// connect with Url and get image stream
			URL url = new URL(imageUrl);
			InputStream in = url.openStream();
			
			String fileNames[] = imageUrl.split("/");
			String fileName = fileNames[fileNames.length-1];
			log.info("fileName: "+fileName);
			
			// generate temp file and add into the document and media, create DLFileEntry
			File file = File.createTempFile(fileName.split("[.]")[0], "."+fileName.split("[.]")[1]);
			file.deleteOnExit();
	        try (FileOutputStream out = new FileOutputStream(file)) {
	            IOUtils.copy(in, out);
	        }
			
	    	DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(userId, groupId, groupId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
	    			file.getName(), MimeTypesUtil.getContentType(file), file.getName(), StringPool.BLANK, StringPool.BLANK, DLFileEntryTypeConstants.FILE_ENTRY_TYPE_ID_BASIC_DOCUMENT, null, file, in, 
	    	                         file.length(), new ServiceContext());
	    	
	    	// update DLFileEntry to update status from draft to approved
	    	dlFileEntry =  DLFileEntryLocalServiceUtil.updateFileEntry(userId, dlFileEntry.getFileEntryId(), file.getName(),
	         		MimeTypesUtil.getContentType(file), file.getName(), StringPool.BLANK, StringPool.BLANK, true, 
	         		dlFileEntry.getFileEntryTypeId(), null, file, null, file.length(), new ServiceContext());
	    	
	    	return dlFileEntry;
		} catch (IOException | PortalException | SystemException e) {
			log.error("Exception while adding image to document and media from url: "+imageUrl, e);
			return null;
		}
	}

	/**
	 * copy DLFileEntry from one group to another group
	 * 
	 * @param oldGroupId
	 * @param uuid
	 * @param group
	 * @return
	 */
	public static DLFileEntry copyImage(long oldGroupId, String uuid, Group group) {
		log.info("Excecuting copyImage for dlFileEntry: "+uuid);
		
		try {
			// get DLFileEntry to copy
			DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntryByUuidAndGroupId(uuid, oldGroupId);
			
			// read its stream and generate temp file and add into the document and media of another group, 
			// create DLFileEntry
			InputStream in = dlFileEntry.getContentStream();
			String fileName = dlFileEntry.getTitle();
			log.info("fileName: "+fileName);
			
			File file = File.createTempFile(fileName.split("[.]")[0], "."+dlFileEntry.getExtension());
			file.deleteOnExit();
	        try (FileOutputStream out = new FileOutputStream(file)) {
	            IOUtils.copy(in, out);
	        }
	        
	        dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(group.getCreatorUserId(), group.getGroupId(), group.getGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
	    			file.getName(), MimeTypesUtil.getContentType(file), file.getName(), StringPool.BLANK, StringPool.BLANK, DLFileEntryTypeConstants.FILE_ENTRY_TYPE_ID_BASIC_DOCUMENT, null, file, in, 
	    	                         file.length(), new ServiceContext());
	    	
	        // update DLFileEntry to update status from draft to approved
	    	dlFileEntry =  DLFileEntryLocalServiceUtil.updateFileEntry(group.getCreatorUserId(), dlFileEntry.getFileEntryId(), file.getName(),
	         		MimeTypesUtil.getContentType(file), file.getName(), StringPool.BLANK, StringPool.BLANK, true, 
	         		dlFileEntry.getFileEntryTypeId(), null, file, null, file.length(), new ServiceContext());
	    	
			return dlFileEntry;
		} catch (SystemException | PortalException | IOException e) {
			log.error("Exception while updating dlFileEntry: "+uuid, e);
			return null;
		}
	}
	
	public static String eventName(String eventName) {
	String eventNameFilter = (eventName.replaceAll("🦁", StringPool.BLANK).replaceAll("📝", StringPool.BLANK).replaceAll("⚡️", StringPool.BLANK).replaceAll("🧙♂️🧙♀️", StringPool.BLANK).replaceAll("₿🍻", StringPool.BLANK).replaceAll("🐛", StringPool.BLANK));
		return eventNameFilter;
	}
}
