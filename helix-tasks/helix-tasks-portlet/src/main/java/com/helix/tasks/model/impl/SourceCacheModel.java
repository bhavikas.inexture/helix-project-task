package com.helix.tasks.model.impl;

import com.helix.tasks.model.Source;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Source in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Source
 * @generated
 */
public class SourceCacheModel implements CacheModel<Source>, Externalizable {
    public long sourceId;
    public long groupId;
    public long companyId;
    public long userId;
    public long createDate;
    public long modifiedDate;
    public String eventSource;
    public String eventSourceLink;
    public String imageEsl;
    public String organizer;
    public String city;
    public String country;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{sourceId=");
        sb.append(sourceId);
        sb.append(", groupId=");
        sb.append(groupId);
        sb.append(", companyId=");
        sb.append(companyId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", eventSource=");
        sb.append(eventSource);
        sb.append(", eventSourceLink=");
        sb.append(eventSourceLink);
        sb.append(", imageEsl=");
        sb.append(imageEsl);
        sb.append(", organizer=");
        sb.append(organizer);
        sb.append(", city=");
        sb.append(city);
        sb.append(", country=");
        sb.append(country);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Source toEntityModel() {
        SourceImpl sourceImpl = new SourceImpl();

        sourceImpl.setSourceId(sourceId);
        sourceImpl.setGroupId(groupId);
        sourceImpl.setCompanyId(companyId);
        sourceImpl.setUserId(userId);

        if (createDate == Long.MIN_VALUE) {
            sourceImpl.setCreateDate(null);
        } else {
            sourceImpl.setCreateDate(new Date(createDate));
        }

        if (modifiedDate == Long.MIN_VALUE) {
            sourceImpl.setModifiedDate(null);
        } else {
            sourceImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (eventSource == null) {
            sourceImpl.setEventSource(StringPool.BLANK);
        } else {
            sourceImpl.setEventSource(eventSource);
        }

        if (eventSourceLink == null) {
            sourceImpl.setEventSourceLink(StringPool.BLANK);
        } else {
            sourceImpl.setEventSourceLink(eventSourceLink);
        }

        if (imageEsl == null) {
            sourceImpl.setImageEsl(StringPool.BLANK);
        } else {
            sourceImpl.setImageEsl(imageEsl);
        }

        if (organizer == null) {
            sourceImpl.setOrganizer(StringPool.BLANK);
        } else {
            sourceImpl.setOrganizer(organizer);
        }

        if (city == null) {
            sourceImpl.setCity(StringPool.BLANK);
        } else {
            sourceImpl.setCity(city);
        }

        if (country == null) {
            sourceImpl.setCountry(StringPool.BLANK);
        } else {
            sourceImpl.setCountry(country);
        }

        sourceImpl.resetOriginalValues();

        return sourceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        sourceId = objectInput.readLong();
        groupId = objectInput.readLong();
        companyId = objectInput.readLong();
        userId = objectInput.readLong();
        createDate = objectInput.readLong();
        modifiedDate = objectInput.readLong();
        eventSource = objectInput.readUTF();
        eventSourceLink = objectInput.readUTF();
        imageEsl = objectInput.readUTF();
        organizer = objectInput.readUTF();
        city = objectInput.readUTF();
        country = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(sourceId);
        objectOutput.writeLong(groupId);
        objectOutput.writeLong(companyId);
        objectOutput.writeLong(userId);
        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifiedDate);

        if (eventSource == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(eventSource);
        }

        if (eventSourceLink == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(eventSourceLink);
        }

        if (imageEsl == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(imageEsl);
        }

        if (organizer == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(organizer);
        }

        if (city == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(city);
        }

        if (country == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(country);
        }
    }
}
