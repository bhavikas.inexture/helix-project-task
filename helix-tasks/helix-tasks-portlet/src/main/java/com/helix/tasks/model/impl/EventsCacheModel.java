package com.helix.tasks.model.impl;

import com.helix.tasks.model.Events;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Events in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Events
 * @generated
 */
public class EventsCacheModel implements CacheModel<Events>, Externalizable {
    public long eventId;
    public long sourceId;
    public long groupId;
    public long companyId;
    public long userId;
    public long createDate;
    public long modifiedDate;
    public String url;
    public String name;
    public String time;
    public String location;
    public String imageEu;
    public String relativeLinkToSite;
    public String siteUuid;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(29);

        sb.append("{eventId=");
        sb.append(eventId);
        sb.append(", sourceId=");
        sb.append(sourceId);
        sb.append(", groupId=");
        sb.append(groupId);
        sb.append(", companyId=");
        sb.append(companyId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", createDate=");
        sb.append(createDate);
        sb.append(", modifiedDate=");
        sb.append(modifiedDate);
        sb.append(", url=");
        sb.append(url);
        sb.append(", name=");
        sb.append(name);
        sb.append(", time=");
        sb.append(time);
        sb.append(", location=");
        sb.append(location);
        sb.append(", imageEu=");
        sb.append(imageEu);
        sb.append(", relativeLinkToSite=");
        sb.append(relativeLinkToSite);
        sb.append(", siteUuid=");
        sb.append(siteUuid);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Events toEntityModel() {
        EventsImpl eventsImpl = new EventsImpl();

        eventsImpl.setEventId(eventId);
        eventsImpl.setSourceId(sourceId);
        eventsImpl.setGroupId(groupId);
        eventsImpl.setCompanyId(companyId);
        eventsImpl.setUserId(userId);

        if (createDate == Long.MIN_VALUE) {
            eventsImpl.setCreateDate(null);
        } else {
            eventsImpl.setCreateDate(new Date(createDate));
        }

        if (modifiedDate == Long.MIN_VALUE) {
            eventsImpl.setModifiedDate(null);
        } else {
            eventsImpl.setModifiedDate(new Date(modifiedDate));
        }

        if (url == null) {
            eventsImpl.setUrl(StringPool.BLANK);
        } else {
            eventsImpl.setUrl(url);
        }

        if (name == null) {
            eventsImpl.setName(StringPool.BLANK);
        } else {
            eventsImpl.setName(name);
        }

        if (time == null) {
            eventsImpl.setTime(StringPool.BLANK);
        } else {
            eventsImpl.setTime(time);
        }

        if (location == null) {
            eventsImpl.setLocation(StringPool.BLANK);
        } else {
            eventsImpl.setLocation(location);
        }

        if (imageEu == null) {
            eventsImpl.setImageEu(StringPool.BLANK);
        } else {
            eventsImpl.setImageEu(imageEu);
        }

        if (relativeLinkToSite == null) {
            eventsImpl.setRelativeLinkToSite(StringPool.BLANK);
        } else {
            eventsImpl.setRelativeLinkToSite(relativeLinkToSite);
        }

        if (siteUuid == null) {
            eventsImpl.setSiteUuid(StringPool.BLANK);
        } else {
            eventsImpl.setSiteUuid(siteUuid);
        }

        eventsImpl.resetOriginalValues();

        return eventsImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        eventId = objectInput.readLong();
        sourceId = objectInput.readLong();
        groupId = objectInput.readLong();
        companyId = objectInput.readLong();
        userId = objectInput.readLong();
        createDate = objectInput.readLong();
        modifiedDate = objectInput.readLong();
        url = objectInput.readUTF();
        name = objectInput.readUTF();
        time = objectInput.readUTF();
        location = objectInput.readUTF();
        imageEu = objectInput.readUTF();
        relativeLinkToSite = objectInput.readUTF();
        siteUuid = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(eventId);
        objectOutput.writeLong(sourceId);
        objectOutput.writeLong(groupId);
        objectOutput.writeLong(companyId);
        objectOutput.writeLong(userId);
        objectOutput.writeLong(createDate);
        objectOutput.writeLong(modifiedDate);

        if (url == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(url);
        }

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (time == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(time);
        }

        if (location == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(location);
        }

        if (imageEu == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(imageEu);
        }

        if (relativeLinkToSite == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(relativeLinkToSite);
        }

        if (siteUuid == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(siteUuid);
        }
    }
}
