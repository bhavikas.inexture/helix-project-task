package com.helix.tasks.model.impl;

/**
 * The extended model implementation for the Source service. Represents a row in the &quot;Helix_Source&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.helix.tasks.model.Source} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class SourceImpl extends SourceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a source model instance should use the {@link com.helix.tasks.model.Source} interface instead.
     */
    public SourceImpl() {
    }
}
