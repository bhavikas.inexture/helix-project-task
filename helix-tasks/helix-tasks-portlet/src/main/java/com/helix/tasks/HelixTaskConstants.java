package com.helix.tasks;

import com.liferay.portal.kernel.util.PropsUtil;

public class HelixTaskConstants {
	public static final String PORTLET_ID = "helixtasks_WAR_helixtasksportlet";
	public static final String CUSTOM_PUBLIC_PAGES_TEMPLATE = PropsUtil.contains("cusotm.public.pages.template") ? PropsUtil.get("cusotm.public.pages.template") : "Public Pages";
	public static final String CUSTOM_PRIVATE_PAGES_TEMPLATE = PropsUtil.contains("cusotm.private.pages.template") ? PropsUtil.get("cusotm.private.pages.template") : "Private Pages"; 
	public static final String CSV_FILE_HEADERS = PropsUtil.contains("csv.file.headers") ? PropsUtil.get("csv.file.headers") : "[event_source, event_source_link]";
}
