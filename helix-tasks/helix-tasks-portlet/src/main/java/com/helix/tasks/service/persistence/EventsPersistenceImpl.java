package com.helix.tasks.service.persistence;

import com.helix.tasks.NoSuchEventsException;
import com.helix.tasks.model.Events;
import com.helix.tasks.model.impl.EventsImpl;
import com.helix.tasks.model.impl.EventsModelImpl;
import com.helix.tasks.service.persistence.EventsPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the events service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsPersistence
 * @see EventsUtil
 * @generated
 */
public class EventsPersistenceImpl extends BasePersistenceImpl<Events>
    implements EventsPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link EventsUtil} to access the events persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = EventsImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, EventsImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, EventsImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SOURCEID = new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, EventsImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySourceId",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOURCEID =
        new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, EventsImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySourceId",
            new String[] { Long.class.getName() },
            EventsModelImpl.SOURCEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_SOURCEID = new FinderPath(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySourceId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_SOURCEID_SOURCEID_2 = "events.sourceId = ?";
    private static final String _SQL_SELECT_EVENTS = "SELECT events FROM Events events";
    private static final String _SQL_SELECT_EVENTS_WHERE = "SELECT events FROM Events events WHERE ";
    private static final String _SQL_COUNT_EVENTS = "SELECT COUNT(events) FROM Events events";
    private static final String _SQL_COUNT_EVENTS_WHERE = "SELECT COUNT(events) FROM Events events WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "events.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Events exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Events exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(EventsPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "time"
            });
    private static Events _nullEvents = new EventsImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Events> toCacheModel() {
                return _nullEventsCacheModel;
            }
        };

    private static CacheModel<Events> _nullEventsCacheModel = new CacheModel<Events>() {
            @Override
            public Events toEntityModel() {
                return _nullEvents;
            }
        };

    public EventsPersistenceImpl() {
        setModelClass(Events.class);
    }

    /**
     * Returns all the eventses where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @return the matching eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findBySourceId(long sourceId) throws SystemException {
        return findBySourceId(sourceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the eventses where sourceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param sourceId the source ID
     * @param start the lower bound of the range of eventses
     * @param end the upper bound of the range of eventses (not inclusive)
     * @return the range of matching eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findBySourceId(long sourceId, int start, int end)
        throws SystemException {
        return findBySourceId(sourceId, start, end, null);
    }

    /**
     * Returns an ordered range of all the eventses where sourceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param sourceId the source ID
     * @param start the lower bound of the range of eventses
     * @param end the upper bound of the range of eventses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findBySourceId(long sourceId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOURCEID;
            finderArgs = new Object[] { sourceId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SOURCEID;
            finderArgs = new Object[] { sourceId, start, end, orderByComparator };
        }

        List<Events> list = (List<Events>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Events events : list) {
                if ((sourceId != events.getSourceId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_EVENTS_WHERE);

            query.append(_FINDER_COLUMN_SOURCEID_SOURCEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(EventsModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(sourceId);

                if (!pagination) {
                    list = (List<Events>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Events>(list);
                } else {
                    list = (List<Events>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first events in the ordered set where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching events
     * @throws com.helix.tasks.NoSuchEventsException if a matching events could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events findBySourceId_First(long sourceId,
        OrderByComparator orderByComparator)
        throws NoSuchEventsException, SystemException {
        Events events = fetchBySourceId_First(sourceId, orderByComparator);

        if (events != null) {
            return events;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("sourceId=");
        msg.append(sourceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchEventsException(msg.toString());
    }

    /**
     * Returns the first events in the ordered set where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching events, or <code>null</code> if a matching events could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events fetchBySourceId_First(long sourceId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Events> list = findBySourceId(sourceId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last events in the ordered set where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching events
     * @throws com.helix.tasks.NoSuchEventsException if a matching events could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events findBySourceId_Last(long sourceId,
        OrderByComparator orderByComparator)
        throws NoSuchEventsException, SystemException {
        Events events = fetchBySourceId_Last(sourceId, orderByComparator);

        if (events != null) {
            return events;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("sourceId=");
        msg.append(sourceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchEventsException(msg.toString());
    }

    /**
     * Returns the last events in the ordered set where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching events, or <code>null</code> if a matching events could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events fetchBySourceId_Last(long sourceId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBySourceId(sourceId);

        if (count == 0) {
            return null;
        }

        List<Events> list = findBySourceId(sourceId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the eventses before and after the current events in the ordered set where sourceId = &#63;.
     *
     * @param eventId the primary key of the current events
     * @param sourceId the source ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next events
     * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events[] findBySourceId_PrevAndNext(long eventId, long sourceId,
        OrderByComparator orderByComparator)
        throws NoSuchEventsException, SystemException {
        Events events = findByPrimaryKey(eventId);

        Session session = null;

        try {
            session = openSession();

            Events[] array = new EventsImpl[3];

            array[0] = getBySourceId_PrevAndNext(session, events, sourceId,
                    orderByComparator, true);

            array[1] = events;

            array[2] = getBySourceId_PrevAndNext(session, events, sourceId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Events getBySourceId_PrevAndNext(Session session, Events events,
        long sourceId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_EVENTS_WHERE);

        query.append(_FINDER_COLUMN_SOURCEID_SOURCEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(EventsModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(sourceId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(events);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Events> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the eventses where sourceId = &#63; from the database.
     *
     * @param sourceId the source ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBySourceId(long sourceId) throws SystemException {
        for (Events events : findBySourceId(sourceId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(events);
        }
    }

    /**
     * Returns the number of eventses where sourceId = &#63;.
     *
     * @param sourceId the source ID
     * @return the number of matching eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBySourceId(long sourceId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_SOURCEID;

        Object[] finderArgs = new Object[] { sourceId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_EVENTS_WHERE);

            query.append(_FINDER_COLUMN_SOURCEID_SOURCEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(sourceId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the events in the entity cache if it is enabled.
     *
     * @param events the events
     */
    @Override
    public void cacheResult(Events events) {
        EntityCacheUtil.putResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsImpl.class, events.getPrimaryKey(), events);

        events.resetOriginalValues();
    }

    /**
     * Caches the eventses in the entity cache if it is enabled.
     *
     * @param eventses the eventses
     */
    @Override
    public void cacheResult(List<Events> eventses) {
        for (Events events : eventses) {
            if (EntityCacheUtil.getResult(
                        EventsModelImpl.ENTITY_CACHE_ENABLED, EventsImpl.class,
                        events.getPrimaryKey()) == null) {
                cacheResult(events);
            } else {
                events.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all eventses.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(EventsImpl.class.getName());
        }

        EntityCacheUtil.clearCache(EventsImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the events.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Events events) {
        EntityCacheUtil.removeResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsImpl.class, events.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Events> eventses) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Events events : eventses) {
            EntityCacheUtil.removeResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
                EventsImpl.class, events.getPrimaryKey());
        }
    }

    /**
     * Creates a new events with the primary key. Does not add the events to the database.
     *
     * @param eventId the primary key for the new events
     * @return the new events
     */
    @Override
    public Events create(long eventId) {
        Events events = new EventsImpl();

        events.setNew(true);
        events.setPrimaryKey(eventId);

        return events;
    }

    /**
     * Removes the events with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param eventId the primary key of the events
     * @return the events that was removed
     * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events remove(long eventId)
        throws NoSuchEventsException, SystemException {
        return remove((Serializable) eventId);
    }

    /**
     * Removes the events with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the events
     * @return the events that was removed
     * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events remove(Serializable primaryKey)
        throws NoSuchEventsException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Events events = (Events) session.get(EventsImpl.class, primaryKey);

            if (events == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchEventsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(events);
        } catch (NoSuchEventsException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Events removeImpl(Events events) throws SystemException {
        events = toUnwrappedModel(events);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(events)) {
                events = (Events) session.get(EventsImpl.class,
                        events.getPrimaryKeyObj());
            }

            if (events != null) {
                session.delete(events);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (events != null) {
            clearCache(events);
        }

        return events;
    }

    @Override
    public Events updateImpl(com.helix.tasks.model.Events events)
        throws SystemException {
        events = toUnwrappedModel(events);

        boolean isNew = events.isNew();

        EventsModelImpl eventsModelImpl = (EventsModelImpl) events;

        Session session = null;

        try {
            session = openSession();

            if (events.isNew()) {
                session.save(events);

                events.setNew(false);
            } else {
                session.merge(events);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !EventsModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((eventsModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOURCEID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        eventsModelImpl.getOriginalSourceId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOURCEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOURCEID,
                    args);

                args = new Object[] { eventsModelImpl.getSourceId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SOURCEID, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SOURCEID,
                    args);
            }
        }

        EntityCacheUtil.putResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
            EventsImpl.class, events.getPrimaryKey(), events);

        return events;
    }

    protected Events toUnwrappedModel(Events events) {
        if (events instanceof EventsImpl) {
            return events;
        }

        EventsImpl eventsImpl = new EventsImpl();

        eventsImpl.setNew(events.isNew());
        eventsImpl.setPrimaryKey(events.getPrimaryKey());

        eventsImpl.setEventId(events.getEventId());
        eventsImpl.setSourceId(events.getSourceId());
        eventsImpl.setGroupId(events.getGroupId());
        eventsImpl.setCompanyId(events.getCompanyId());
        eventsImpl.setUserId(events.getUserId());
        eventsImpl.setCreateDate(events.getCreateDate());
        eventsImpl.setModifiedDate(events.getModifiedDate());
        eventsImpl.setUrl(events.getUrl());
        eventsImpl.setName(events.getName());
        eventsImpl.setTime(events.getTime());
        eventsImpl.setLocation(events.getLocation());
        eventsImpl.setImageEu(events.getImageEu());
        eventsImpl.setRelativeLinkToSite(events.getRelativeLinkToSite());
        eventsImpl.setSiteUuid(events.getSiteUuid());

        return eventsImpl;
    }

    /**
     * Returns the events with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the events
     * @return the events
     * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events findByPrimaryKey(Serializable primaryKey)
        throws NoSuchEventsException, SystemException {
        Events events = fetchByPrimaryKey(primaryKey);

        if (events == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchEventsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return events;
    }

    /**
     * Returns the events with the primary key or throws a {@link com.helix.tasks.NoSuchEventsException} if it could not be found.
     *
     * @param eventId the primary key of the events
     * @return the events
     * @throws com.helix.tasks.NoSuchEventsException if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events findByPrimaryKey(long eventId)
        throws NoSuchEventsException, SystemException {
        return findByPrimaryKey((Serializable) eventId);
    }

    /**
     * Returns the events with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the events
     * @return the events, or <code>null</code> if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Events events = (Events) EntityCacheUtil.getResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
                EventsImpl.class, primaryKey);

        if (events == _nullEvents) {
            return null;
        }

        if (events == null) {
            Session session = null;

            try {
                session = openSession();

                events = (Events) session.get(EventsImpl.class, primaryKey);

                if (events != null) {
                    cacheResult(events);
                } else {
                    EntityCacheUtil.putResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
                        EventsImpl.class, primaryKey, _nullEvents);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(EventsModelImpl.ENTITY_CACHE_ENABLED,
                    EventsImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return events;
    }

    /**
     * Returns the events with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param eventId the primary key of the events
     * @return the events, or <code>null</code> if a events with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Events fetchByPrimaryKey(long eventId) throws SystemException {
        return fetchByPrimaryKey((Serializable) eventId);
    }

    /**
     * Returns all the eventses.
     *
     * @return the eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the eventses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of eventses
     * @param end the upper bound of the range of eventses (not inclusive)
     * @return the range of eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the eventses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.helix.tasks.model.impl.EventsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of eventses
     * @param end the upper bound of the range of eventses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Events> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Events> list = (List<Events>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_EVENTS);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_EVENTS;

                if (pagination) {
                    sql = sql.concat(EventsModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Events>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Events>(list);
                } else {
                    list = (List<Events>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the eventses from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Events events : findAll()) {
            remove(events);
        }
    }

    /**
     * Returns the number of eventses.
     *
     * @return the number of eventses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_EVENTS);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the events persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.helix.tasks.model.Events")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Events>> listenersList = new ArrayList<ModelListener<Events>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Events>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(EventsImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
