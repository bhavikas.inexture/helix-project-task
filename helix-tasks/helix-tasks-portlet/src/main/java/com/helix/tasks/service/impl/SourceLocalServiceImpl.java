package com.helix.tasks.service.impl;

import java.util.Date;

import com.helix.tasks.NoSuchSourceException;
import com.helix.tasks.model.Source;
import com.helix.tasks.service.base.SourceLocalServiceBaseImpl;
import com.helix.tasks.service.persistence.SourceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the source local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.helix.tasks.service.SourceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.helix.tasks.service.base.SourceLocalServiceBaseImpl
 * @see com.helix.tasks.service.SourceLocalServiceUtil
 */
public class SourceLocalServiceImpl extends SourceLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.helix.tasks.service.SourceLocalServiceUtil} to access the source local service.
     */
	
	public Source addSource(long companyId, long groupId, long userId, String eventSource, String eventSourceLink, String imageEsl, String organizer, String city, String country, ServiceContext serviceContext) throws PortalException, SystemException {
		long sourceId = counterLocalService.increment(Source.class.getName());
		
		Source source = sourcePersistence.create(sourceId);
		
		source.setCompanyId(companyId);
		source.setGroupId(groupId);
		source.setUserId(userId);
		
		source.setEventSource(eventSource);
		source.setEventSourceLink(eventSourceLink);
		source.setImageEsl(imageEsl);
		source.setOrganizer(organizer);
		source.setCity(city);
		source.setCountry(country);
		
		Date now = new Date();
		source.setCreateDate(serviceContext.getCreateDate(now));
		source.setModifiedDate(serviceContext.getModifiedDate(now));
		
		source = super.addSource(source);
		
		return source;
	}
	
	public Source findByEventSourceLink(String eventSourceLink) throws NoSuchSourceException, SystemException {
		return SourceUtil.findByEventSourceLink(eventSourceLink);
	}
}
