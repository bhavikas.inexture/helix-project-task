create table Helix_Events (
	eventId LONG not null primary key,
	sourceId LONG,
	groupId LONG,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	url TEXT null,
	name TEXT null,
	time_ VARCHAR(75) null,
	location TEXT null,
	imageEu VARCHAR(75) null,
	relativeLinkToSite TEXT null,
	siteUuid VARCHAR(75) null
);

create table Helix_Source (
	sourceId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	eventSource VARCHAR(75) null,
	eventSourceLink TEXT null,
	imageEsl VARCHAR(75) null,
	organizer VARCHAR(75) null,
	city VARCHAR(75) null,
	country VARCHAR(75) null
);