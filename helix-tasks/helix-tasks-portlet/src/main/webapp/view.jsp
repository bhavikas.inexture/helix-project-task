<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css"/>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
 
<portlet:defineObjects />
<liferay-theme:defineObjects/>

<liferay-ui:success key="file-upload-request-completed" message="File is being processed. You will be notified when process gets completed and data are available. Thank You."/>

<liferay-ui:error key="empty-invalid-csv-file" message="Please upload valid CSV file with header." />
<liferay-ui:error key="error-message" message="Something went wrong! Please try again later." />

<c:if test="${permissionChecker.isCompanyAdmin()}">
	<portlet:actionURL var="uploadFile" name="uploadFile" />
	<portlet:resourceURL var="downloadURL"></portlet:resourceURL>
	
	<aui:form action="<%=uploadFile%>" method="POST" enctype="multipart/form-data">
		<aui:row>
			<aui:col span="4">
				<aui:input name="file" type="file" required="true" label="Upload File">
					<aui:validator name="required"/>
					<aui:validator name="acceptFiles">'csv'</aui:validator>
				</aui:input>
			</aui:col>
			<aui:col span="4">
				<aui:button type="submit" primary="true" name="Upload"/>
			</aui:col>
			<aui:col span="4">
				<aui:a href="${downloadURL}" cssClass="btn btn-primary">Download CSV</aui:a>
			</aui:col>
		</aui:row>
	</aui:form>
</c:if>

<table id="records" style="width:100%" class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>Organizer</td>
			<td>City</td>
			<td>Country</td>
			<td>Event Name</td>
			<td>Date & Time</td>
			<td>Location</td>
			<td>Relative Site Link</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${response}" var="response">
			<tr>
				<td>${response.get("organizer")}</td>
				<td>${response.get("city")}</td>
				<td>${response.get("country")}</td>
				<td>${response.get("event_name")}</td>
				<td>${response.get("date_time")}</td>
				<td>${response.get("location")}</td>
				<td>${response.get("relative_link")}</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td>Organizer</td>
			<td>City</td>
			<td>Country</td>
			<td>Event Name</td>
			<td>Date & Time</td>
			<td>Location</td>
			<td>Relative Site Link</td>
		</tr>
	</tfoot>
</table>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>



